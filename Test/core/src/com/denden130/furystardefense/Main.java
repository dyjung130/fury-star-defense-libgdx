package com.denden130.furystardefense;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.mygdx.handler.AndroidCallbackHandler;
import com.mygdx.handler.GoogleServicesHandler;
import com.mygdx.handler.ScreenCallbackHandler;
import com.mygdx.handler.ScrollerCallbackHandler;
import com.mygdx.player.MyPlayer;
import com.mygdx.screen.LeaderboardScreen;
import com.mygdx.screen.LoadingScreen;
import com.mygdx.screen.MainScreen;
import com.mygdx.screen.PlayScreen;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;

public class Main extends Game implements ScreenCallbackHandler, ScrollerCallbackHandler{

	public static int currentView = Constants.loadingScreen;
	private static MyPlayer player = MyPlayer.getInstance();
	private AndroidCallbackHandler acbh;
	public static GoogleServicesHandler gsh;
	
	public Main(GoogleServicesHandler gsh){
		super();
		Main.gsh = gsh;

	}
	
	@Override
	public void create () {
		setScreen(new LoadingScreen(this, player, Constants.mainScreen));
		Gdx.input.setCatchBackKey(true);
	}
	
	public void setCallBack(AndroidCallbackHandler acbh){
		this.acbh = acbh;
	}
	
	 	
	@Override
	public void render () {
		super.render();
	
	}
	
	@Override
	public void selectScreen(int screenId, int toScreenId, MyPlayer player) {
		// TODO Auto-generated method stub
		currentView = toScreenId;
		
		switch(currentView){
		case Constants.loadingScreen:
			setScreen(new LoadingScreen(this, player, toScreenId));
			break;
		case Constants.mainScreen:
			setScreen(new MainScreen(this, player));
			break;
		case Constants.playScreen:
			setScreen(new PlayScreen(this, player));
			break;
		case Constants.lbScreen:
			setScreen(new LeaderboardScreen(this, player));
			break;
		default:
			setScreen(new MainScreen(this, player));
			break;
		}
	}
	

	public void rateGame(){
		gsh.rateGame();
	}
	public void submitScore(int score){
		gsh.submitScore(score);
	}
	
	@Override
	public boolean isSignedIn(){
		return gsh.isSignedIn();
	}
	
	@Override
	public void openLeaderBoard(){
		//check if the user is signed in, if not it will be handled from android side in onActivityResult
		if(gsh.isSignedIn()){
			gsh.showScore();
		}else{
			gsh.signIn();
		}
	}
	
	@Override
	public int getHighestScore(){
		return gsh.getHighestScore();
	}
	
	@Override
	public void connectToGooglePlayServices() {
		// TODO Auto-generated method stub
		gsh.signIn();
		if(gsh.isSignedIn()){
			acbh.loadFromLocalGame(player, Constants.LOCAL_USERDATA);
			
		}
	}

	@Override
	public void dispose(){
		super.dispose();
		Assets.dispose();
	}
	
	@Override
	public void pause(){
		super.pause();
		
	}
	
	@Override
	public void resume(){
		super.resume();
		
	}

	@Override
	public void callMoveBy(float deltaPosX, float deltaPosY) {
		// TODO Auto-generated method stub
		acbh.callBack(deltaPosX);
	}



}
