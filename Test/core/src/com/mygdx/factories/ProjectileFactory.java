package com.mygdx.factories;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.actors.AbstractProjectile;
import com.mygdx.testprojectiles.MeleeProjectile;
import com.mygdx.testprojectiles.ProjectileOne;
import com.mygdx.testprojectiles.ProjectileTwo;


public class ProjectileFactory {

	private static ProjectileFactory tpfInstance = new ProjectileFactory();
	
	private ProjectileFactory(){
		//Singleton pattern
	}
	
	public static synchronized ProjectileFactory getInstance(){
		return tpfInstance;
	}
	
	public AbstractProjectile getTestProjectile(String tp, Vector2 targetPos, Vector2 startPos, int speed){
		AbstractProjectile c = null;
		
		
		if(tp.equalsIgnoreCase(null)){
			c = null;
		}
		
		//for melee unit (no projectile is needed, so goto MeleeProjectile and set texture to transparent image (small one))
		if(tp.equalsIgnoreCase("MELEE")){
			c = new MeleeProjectile(targetPos, startPos, speed);
		}
		
		//for range unit
		if(tp.equalsIgnoreCase("ONE")){
			
			c = new ProjectileOne(targetPos, startPos, speed);
		}
		
		if(tp.equalsIgnoreCase("BLAST")){
			c = new ProjectileTwo(targetPos, startPos, speed);
		}
		
		return c;
	}
}
