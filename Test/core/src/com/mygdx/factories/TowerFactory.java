package com.mygdx.factories;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mygdx.actors.AbstractTower;
import com.mygdx.towers.TowerArea;
import com.mygdx.towers.TowerMulti;
import com.mygdx.towers.TowerSingle;
import com.mygdx.towers.TowerUtil;
import com.mygdx.utils.TowerType;
import com.mygdx.utils.Constants;

public class TowerFactory {

	private static TowerFactory instance;
	
	
	private TowerFactory(){
	/* singleton*/	
	}
	
	public static synchronized TowerFactory getInstance(){
		if(instance == null){
			instance= new TowerFactory();
		}
		return instance;
	}
	
	public AbstractTower getTower(Vector2 startPos, Vector2 dimension, TowerType type){
		AbstractTower actor = null;
	
		switch(type.getId()){
		case Constants.SINGLE_1:
			actor = new TowerSingle(startPos, dimension, type);
			break;
		case Constants.MULTI_1:
			actor = new TowerMulti(startPos, dimension, type);
			break;
		case Constants.AREA_1:
			actor = new TowerArea(startPos, dimension, type);
			break;
		case Constants.UTIL_1:
			actor = new TowerUtil(startPos, dimension, type);
			break;
		default:
			//handle exception
			break;
		}
		
		return actor;
		
	}
	

}
