package com.mygdx.factories;

import com.mygdx.actors.AbstractCreep;
import com.mygdx.creep.Creep;
import com.mygdx.utils.CreepType;

public class CreepFactory {

	private static CreepFactory instance;
	
	private CreepFactory(){
		//singleton pattern
	}
	
	public synchronized static CreepFactory getInstance(){
		//fail-safe check
		if(instance == null){
			instance = new CreepFactory();
		}
		
		return instance;
	}
	
	public AbstractCreep getTestTarget(String target){
		
		assert(target!=null);
		AbstractCreep tta = new Creep(CreepType.valueOf(target).getHealth(), CreepType.valueOf(target).getSpeed(), CreepType.valueOf(target).getGold(), CreepType.valueOf(target).getTexture());
		return tta;
	}
	
	public void clear(){
		if(instance != null)
			instance = null;
	}
	
}
