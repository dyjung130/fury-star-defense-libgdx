package com.mygdx.towers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mygdx.actors.AbstractTowerArea;
import com.mygdx.userinterface.ResearchUI;
import com.mygdx.utils.TowerType;

public class TowerArea extends AbstractTowerArea{

	
	private Image image;
	private Texture texture;
	private float selectedTimer = 5f;
	
	public TowerArea(Vector2 startPos, Vector2 dimension, TowerType type){
		super(startPos, dimension, type.getRange(), type.getDamage(), type.getCost(), type.getFireRate()
				,type.getBulletSpeed(), type.getCD(), type.getEffectType());
		texture = type.getTexture();
	
		this.image = new Image(texture);
		image.setBounds(xPos, yPos, width, height);
		image.setOrigin(width/2, height/2);
		image.addAction(Actions.forever(Actions.sequence(Actions.sizeBy(-4, -4, 1f), Actions.sizeBy(4, 4, 1f))));
		addListener(new ClickListener(){
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor){
			//	if(TopStage.botStageDragged == true)
				super.enter(event, x, y, pointer, fromActor);
				showProperties();
			}
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				//to upgrade the tower
				super.touchDown(event, x, y, pointer, button);
				
				showProperties();
				return false;
			}
		});	

	}
	
	
	private void showProperties(){
		ResearchUI.getInstance().setUpgradeProperties(texture, damage, range, towerLevel, 0, cost, effectType.name());//, getProp()[1], getProp()[2]);
		ResearchUI.getInstance().setEvolveProperties(this, towerID);
	}
	
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		image.draw(batch, parentAlpha);
		
	}

	@Override
	public void act(float delta){
		super.act(delta);
		image.act(delta);
		image.setRotation(super.towerAngle);
			
	}

	@Override
	public void upgrade(){
		super.upgrade();
		showProperties();
	}
	
	@Override
	public void evolveToOne(){
		towerLevel = 1;
		towerID = TowerType.AREA1E.getId();
		range = TowerType.AREA1E.getRange();
		damage = TowerType.AREA1E.getDamage();
		cost = TowerType.AREA1E.getCost();
		duration = TowerType.AREA1E.getBulletSpeed();
		fireRate = TowerType.AREA1E.getFireRate();
		texture = TowerType.AREA1E.getTexture();
		effectType = TowerType.AREA1E.getEffectType();
		image.setDrawable(new SpriteDrawable(new Sprite(texture)));
		pe = new ParticleEffect();
		pe.load(Gdx.files.internal("Effect/blast.p"),Gdx.files.internal("Effect"));
		pe.scaleEffect(3f);
		pe.getEmitters().first().setPosition(centerX , centerY);
		pe.setPosition(centerX, centerY);
		pe.start();
		showProperties();
	}
	
	@Override
	public void evolveToTwo(){
		towerLevel = 1;
		towerID = TowerType.AREA2E.getId();
		range = TowerType.AREA2E.getRange();
		damage = TowerType.AREA2E.getDamage();
		cost = TowerType.AREA2E.getCost();
		duration = TowerType.AREA2E.getBulletSpeed();
		fireRate = TowerType.AREA2E.getFireRate();
		texture = TowerType.AREA2E.getTexture();
		effectType = TowerType.AREA2E.getEffectType();
		image.setDrawable(new SpriteDrawable(new Sprite(texture)));
		pe = new ParticleEffect();
		pe.load(Gdx.files.internal("Effect/timeEffect.p"),Gdx.files.internal("Effect"));
		pe.scaleEffect(2f);
		pe.getEmitters().first().setPosition(centerX , centerY);
		pe.setPosition(centerX, centerY);
		pe.start();
		showProperties();
		
		
	}


}
