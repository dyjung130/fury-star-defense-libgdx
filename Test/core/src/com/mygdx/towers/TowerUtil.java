package com.mygdx.towers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mygdx.actors.AbstractTowerUtil;
import com.mygdx.stage.TopStage;
import com.mygdx.userinterface.ResearchUI;
import com.mygdx.utils.Constants;
import com.mygdx.utils.EffectType;
import com.mygdx.utils.TowerType;

public class TowerUtil extends AbstractTowerUtil{

	private Image image;
	private Texture texture;

	
	public TowerUtil(Vector2 startPos, Vector2 dimension, TowerType type){
		super(startPos, dimension, type.getCost(), type.getCD(), type.getEffectType());
		effect = type.getEffectType();
		texture = type.getTexture();
		collectible = type.getCollectible();
		baseCollectible = collectible;
		image = new Image(texture);
		image.setBounds(xPos, yPos, width, height);
		image.setOrigin(width/2, height/2);
		image.addAction(Actions.forever(Actions.sequence(Actions.sizeBy(-4, -4, 1f), Actions.sizeBy(4, 4, 1f))));
		
		addListener(new ClickListener(){
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor){
			super.enter(event, x, y, pointer, fromActor);
				showProperties();
			
			}
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				//to upgrade the tower
				super.touchDown(event, x, y, pointer, button);
				showProperties();
				return false;
			}
		});

	}

	private void showProperties(){
		ResearchUI.getInstance().setUpgradeProperties(texture, collectible, 0, towerLevel, 0, cost, effect.name());//, getProp()[1], getProp()[2]);
		ResearchUI.getInstance().setEvolveProperties(this, towerID);
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		image.act(delta);
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		image.draw(batch, parentAlpha);
	}

	@Override
	public void upgrade() {
		// TODO Auto-generated method stub
		super.upgrade();
		showProperties();
	}
	
	@Override
	public void evolveToOne(){
		towerLevel = 1;
		towerID = TowerType.UTIL1E.getId();
		cost = TowerType.UTIL1E.getCost();
		cost = TowerType.UTIL1E.getCost();
		collectible = TowerType.UTIL1E.getCollectible();
		baseCollectible = TowerType.UTIL1E.getCollectible();
		texture = TowerType.UTIL1E.getTexture();
		effect = TowerType.UTIL1E.getEffectType();
		image.setDrawable(new SpriteDrawable(new Sprite(texture)));
		pe = new ParticleEffect();
		pe.load(Gdx.files.internal("Effect/coinCollect.p"),Gdx.files.internal("Effect"));
		pe.scaleEffect(2.5f);
		pe.getEmitters().first().setPosition(centerX , centerY);
		pe.setPosition(centerX, centerY);
		pe.start();
		showProperties();
	}
	
	@Override
	public void evolveToTwo(){
		towerLevel = 1;
		towerID = TowerType.UTIL2E.getId();
		cost = TowerType.UTIL2E.getCost();
		cost = TowerType.UTIL2E.getCost();
		collectible = TowerType.UTIL2E.getCollectible();
		baseCollectible = TowerType.UTIL2E.getCollectible();
		collectingRate = Constants.tCollectibleRate;
		texture = TowerType.UTIL2E.getTexture();
		effect = TowerType.UTIL2E.getEffectType();
		image.setDrawable(new SpriteDrawable(new Sprite(texture)));
		pe = new ParticleEffect();
		pe.load(Gdx.files.internal("Effect/sparkling.p"),Gdx.files.internal("Effect"));
		pe.scaleEffect(1.4f);
		pe.getEmitters().first().setPosition(centerX , centerY);
		pe.setPosition(centerX, centerY);
		pe.start();
		showProperties();
		
	}

	@Override
	public void towerActivated() {
	
		//Update money by default subract the passed value so negate it to increment
		if(!(effect==EffectType.GOLDSCORE)){
			TopStage.getInstance().updateMoney(-collectible); //update money only
		}else{
			TopStage.getInstance().updateScore(collectible);//update score
			TopStage.getInstance().updateMoney(-collectible);//update money
		}
	}

}
