package com.mygdx.towers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mygdx.actors.AbstractTowerSingle;
import com.mygdx.userinterface.ResearchUI;
import com.mygdx.utils.TowerType;

public class TowerSingle extends AbstractTowerSingle{

	private Image image;
	private Texture texture;

	public TowerSingle(Vector2 startPos, Vector2 dimension, TowerType type){
		super(startPos, dimension, type.getRange(), type.getDamage(), type.getCost(), type.getFireRate(), type.getBulletSpeed(),
				"ONE", type.getCD(), type.getEffectType());
	
		//replaced with image from texture (allows to rotate the image based on the direction (atan))
		texture = type.getTexture();
		
		image = new Image(texture);
		image.setBounds(xPos, yPos, width, height);
		image.setOrigin(width/2, height/2);
		image.addAction(Actions.forever(Actions.sequence(Actions.sizeBy(-4, -4, 1f), Actions.sizeBy(4, 4, 1f))));
		
		addListener(new ClickListener(){
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor){
				super.enter(event, x, y, pointer, fromActor);
				showProperties();
			
			}
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				//to upgrade the tower
				super.touchDown(event, x, y, pointer, button);
				showProperties();
				return false;
			}
		});

	}
	
	private void showProperties(){
		ResearchUI.getInstance().setUpgradeProperties(texture, damage, range, towerLevel, 0, cost, effectType.name());//, getProp()[1], getProp()[2]);
		ResearchUI.getInstance().setEvolveProperties(this, towerID);
	}
	
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		image.draw(batch, parentAlpha);
	}

	@Override
	public void act(float delta){
		super.act(delta);
		image.act(delta);
		image.setRotation(super.towerAngle);

	}

	@Override
	public void upgrade() {
		// TODO Auto-generated method stub
		super.upgrade();
		showProperties();
	}
	
	@Override
	public void evolveToOne(){
		towerLevel = 1;
		towerID = TowerType.SINGLE1E.getId();
		range = TowerType.SINGLE1E.getRange();
		damage = TowerType.SINGLE1E.getDamage();
		cost = TowerType.SINGLE1E.getCost();
		bulletSpeed = TowerType.SINGLE1E.getBulletSpeed();
		fireRate = TowerType.SINGLE1E.getFireRate();
		texture = TowerType.SINGLE1E.getTexture();
		effectType= TowerType.SINGLE1E.getEffectType();
		image.setDrawable(new SpriteDrawable(new Sprite(texture)));
		projectileType = "ONE";
		showProperties();
	}
	
	@Override
	public void evolveToTwo(){
		towerLevel = 1;
		towerID = TowerType.SINGLE2E.getId();
		range = TowerType.SINGLE2E.getRange();
		damage = TowerType.SINGLE2E.getDamage();
		cost = TowerType.SINGLE2E.getCost();
		bulletSpeed = TowerType.SINGLE2E.getBulletSpeed();
		fireRate = TowerType.SINGLE2E.getFireRate();
		texture = TowerType.SINGLE2E.getTexture();
		effectType= TowerType.SINGLE2E.getEffectType();
		image.setDrawable(new SpriteDrawable(new Sprite(texture)));
		projectileType = "ONE";
		pe = new ParticleEffect();
		pe.load(Gdx.files.internal("Effect/thunderEffect.p"),Gdx.files.internal("Effect"));
		pe.scaleEffect(1.0f);
		pe.getEmitters().first().setPosition(centerX , centerY);
		pe.setPosition(centerX, centerY);
		pe.start();
		showProperties();
	}
}
