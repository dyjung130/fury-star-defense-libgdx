package com.mygdx.towers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mygdx.actors.AbstractTowerMulti;
import com.mygdx.userinterface.ResearchUI;
import com.mygdx.utils.TowerType;

public class TowerMulti extends AbstractTowerMulti{

	
	private Image image;
	private Texture texture;

	public TowerMulti(Vector2 startPos, Vector2 dim, TowerType type){
		super(startPos, dim, type.getRange(), type.getDamage(), type.getCost(),type.getFireRate(), type.getBulletSpeed(),
				"ONE", type.getCD(), type.getEffectType(), type.getNumOfTargets());
		
		//replaced with image from texture (allows to rotate the image based on the direction (atan))
		texture = type.getTexture();
		
		image = new Image(texture);
		image.setBounds(xPos, yPos, width, height);
		image.setOrigin(width/2, height/2);
		image.addAction(Actions.forever(Actions.sequence(Actions.sizeBy(-4, -4, 1f), Actions.sizeBy(4, 4, 1f))));
		addListener(new ClickListener(){
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor){
		
				super.enter(event, x, y, pointer, fromActor);
				showProperties();
			
			}
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				//to upgrade the tower
				super.touchDown(event, x, y, pointer, button);
				showProperties();
				return false;
			}
		});	

	}

	private void showProperties(){
		ResearchUI.getInstance().setUpgradeProperties(texture, damage, range, towerLevel, 0, cost, effectType.name());//, getProp()[1], getProp()[2]);
		ResearchUI.getInstance().setEvolveProperties(this, towerID);
	}
	
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		image.draw(batch, parentAlpha);
	
	}

	@Override
	public void act(float delta){
		super.act(delta);
		image.act(delta);
		image.setRotation(super.towerAngle);
	}

	@Override
	public void upgrade(){
		super.upgrade();
		showProperties();
	}
	
	@Override
	public void evolveToOne(){
		towerLevel = 1;
		towerID = TowerType.MULTI1E.getId();
		range = TowerType.MULTI1E.getRange();
		damage = TowerType.MULTI1E.getDamage();
		cost = TowerType.MULTI1E.getCost();
		bulletSpeed = TowerType.MULTI1E.getBulletSpeed();
		fireRate = TowerType.MULTI1E.getFireRate();
		texture = TowerType.MULTI1E.getTexture();
		numOfTargets = TowerType.MULTI1E.getNumOfTargets();
		effectType = TowerType.MULTI1E.getEffectType();
		image.setDrawable(new SpriteDrawable(new Sprite(texture)));
		pe.load(Gdx.files.internal("Effect/blade.p"),Gdx.files.internal("Effect"));
		pe.scaleEffect(0.8f);
		pe.getEmitters().first().setPosition(centerX , centerY);
		pe.setPosition(centerX, centerY);
		pe.start();
		showProperties();
	}
	
	@Override
	public void evolveToTwo(){
		towerLevel = 1;
		towerID = TowerType.MULTI2E.getId();
		range = TowerType.MULTI2E.getRange();
		damage = TowerType.MULTI2E.getDamage();
		cost = TowerType.MULTI2E.getCost();
		bulletSpeed = TowerType.MULTI2E.getBulletSpeed();
		fireRate = TowerType.MULTI2E.getFireRate();
		texture = TowerType.MULTI2E.getTexture();
		numOfTargets = TowerType.MULTI2E.getNumOfTargets();
		effectType = TowerType.MULTI2E.getEffectType();
		image.setDrawable(new SpriteDrawable(new Sprite(texture)));
		showProperties();
		
	}
}
