package com.mygdx.testprojectiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.actors.AbstractProjectile;
import com.mygdx.actors.AbstractCreep;
import com.mygdx.utils.Assets;

public class MeleeProjectile extends AbstractProjectile{

	public MeleeProjectile(Vector2 targetPos, Vector2 startPos, int speed) {
		//change image with small transparent one
		super(targetPos, startPos, Assets.getAssetManager().get("projectile.jpg", Texture.class), speed);
		
	}

	

}
