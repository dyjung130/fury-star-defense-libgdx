package com.mygdx.testprojectiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.actors.AbstractProjectile;
import com.mygdx.utils.Assets;

public class ProjectileOne extends AbstractProjectile{

	private ParticleEffect pe;
	float tempX, tempY;

	public ProjectileOne(Vector2 targetPos, Vector2 startPos, int speed) {
		super(targetPos, startPos, Assets.getAssetManager().get("projectile.png", Texture.class), speed);
		pe = new ParticleEffect();
	    pe.load(Gdx.files.internal("Effect/fireBall.p"),Gdx.files.internal("Effect"));
	    pe.getEmitters().first().setPosition(startPos.x , startPos.y);
	    pe.setPosition(startPos.x, startPos.y);
	    pe.scaleEffect(0.8f);
	    pe.start();
	    tempX = startPos.x; tempY = startPos.y;
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		if(isVisible()==true){
			pe.draw(batch);
		}else{
			currentPos.x = tempX;
			currentPos.y = tempY;
			pe.reset();
		}
		pe.update(Gdx.graphics.getDeltaTime());
		pe.setPosition(currentPos.x, currentPos.y);
	}
	
	@Override
	public void clear(){
		super.clear();
	}

}
