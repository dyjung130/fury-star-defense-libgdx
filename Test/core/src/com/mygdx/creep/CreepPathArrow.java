package com.mygdx.creep;

import java.util.List;
import java.util.Random;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mygdx.actors.BackGround;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;

public class CreepPathArrow extends Actor{

	private List<Vector2> nodeAList;
	private Vector2[] nodeList;// = new Vector2[4];
	private int n = 0; //node counter
	private Image arrowImage;
	private boolean reachedDestination;
	private Vector2 v = new Vector2(0,Constants.MAP_BOTTOM); 
	private final int movementSpeed = 500;
	private int centerX = Constants.ARROW_DIM/2, centerY =Constants.ARROW_DIM/2;
	private Random rand = new Random();
	
	public CreepPathArrow(){
		
		setNodes();
	}
	public void setNodes(){
		arrowImage = new Image(Assets.getAssetManager().get("Icons/pathArrow.png", Texture.class));
		arrowImage.setBounds(0, 0, Constants.ARROW_DIM, Constants.ARROW_DIM);
		arrowImage.setOrigin(centerX, centerY);
		arrowImage.setColor(Color.YELLOW);
		nodeAList = BackGround.getInstance().getNodesfromMap();
		nodeList = new Vector2[nodeAList.size()];
		nodeList = nodeAList.toArray(nodeList);
	
	}
	
	private void changeDirection(float degrees){
	
			arrowImage.setRotation(degrees);
	}
		
		//move target(imageTarget) and rotate target's direction
	private void moveTarget(float movementSpeed){
		if(n != nodeList.length){//test..
			for(; n < nodeList.length;){
				
				if((int)Math.abs(nodeList[n].x - v.x) >= 10){
					float tempX = (nodeList[n].x - v.x)/Math.abs(v.x - nodeList[n].x);
					v.x += tempX * movementSpeed ;
					
					if(tempX > 0){
						changeDirection(270); // right
					}
						
					if(tempX < 0){
						changeDirection(90); //  left
					}
				}
						
				if((int)Math.abs(nodeList[n].y - v.y) >= 10){
					float tempY = (nodeList[n].y - v.y)/Math.abs(v.y - nodeList[n].y);
					v.y +=  tempY * movementSpeed;
						
					if(tempY > 0){
						changeDirection(0); // up
					}
						
					if(tempY < 0){
						changeDirection(180); // down
					}
				}
						
				if((int)Math.abs(nodeList[n].x - v.x) <= 10 && (int)Math.abs(nodeList[n].y - v.y) <= 10){
						n ++;
				}
					
				arrowImage.setPosition(v.x, v.y);
				break;
			}
			
		}else{
			reachedDestination = true;
	
		}
		
	}
	

	public boolean isReachedDestination(){
		return reachedDestination;
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		if(reachedDestination == false)
			moveTarget(delta*movementSpeed);
	}
		
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		if(reachedDestination == false)
			arrowImage.draw(batch, parentAlpha);
	}
		
	@Override 
	public boolean remove(){
		return super.remove();
	}
		
	@Override 
	public void clear(){
		super.clear();
		arrowImage.remove();
	}
}
