package com.mygdx.creep;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.mygdx.factories.CreepFactory;
import com.mygdx.actors.AbstractCreep;
import com.mygdx.utils.Constants;

public class EnemyWave {

	//class that returns enemyList
	/*
	 * Below is testing where getter methods cause delay.
	 * needs to implement below
	 * 1) Call setter(int wave)
	 * 2) If both lists are empty, prepare two EnemyList for (1) wave & (2) wave + 1
	 * 3) Call getter and return the EnemyList for (1) wave
	 * 4) After return clear EnemyList (1) wave
	 * 5) Set EnemyList (2) wave + 1 = EnemyList(1) in the clear method and clear EnemyList(2) wav +1
	 * 6) If wave + 1 is empty, construct EnemyLIst(2) wave + 1 an repeat 4-6.
	 */
	/*private static int[][] creepData = new int[100][20];
	private static final int baseNumOfCreep = 7;
	private static final int creepNumRate = 2;
	private static final float [][] probPerLevel = {
		{1}, //level 1
		{0.9f, 0.1f},//level 2
		{0.8f, 0.2f},//level 3
		{0.7f, 0.3f},//level 4
		{0.6f, 0.3f, 0.1f},//level 5
		{0.5f, 0.3f, 0.2f},//level 6
		{0.4f, 0.3f, 0.2f, 0.1f},//level 7
		{0.3f, 0.3f, 0.2f, 0.1f, 0.1f},//level 8
		{0.2f, 0.2f, 0.2f, 0.2f, 0.2f}//level 9
	};
	
	static{
		
		int baseCreepLvl = 0;
		
		for(int level = 0; level < creepData.length; level++){
			for(int creepType = 0; creepType < creepData[0].length; creepType++){
				//needs an equation to generate the creepdata
				int totalNumOfCreep = 0;
				
				if(level > 0 && level % 10 == 0){
					totalNumOfCreep = 1; // boss level (every 10 +)
				}else if ( !(level % 10 == 0)){
					totalNumOfCreep = baseNumOfCreep + (creepNumRate * (level%10)); //from 1 ~ 9
				}else{
					totalNumOfCreep = baseNumOfCreep; // start (level = 0)
				}
				
				if(totalNumOfCreep > 0 && !(level < baseCreepLvl)){
					int creepSize = (int) (totalNumOfCreep * probPerLevel[level % 0][baseCreepLvl]);
					creepData[level][creepType] = creepSize;
					totalNumOfCreep -= creepSize;
					baseCreepLvl += 1;
				}else{
					creepData[level][creepType] = 0;//initialize to 0
				}
			}
			
			if(!(level > 70)){
				baseCreepLvl -= 3;
			}else{
				baseCreepLvl -= 4;
			}
		}
	}
	
	public static final int[][] enemyWaveList = {{2, 3, 4 }, //lvl 1
		  {1, 10, 4,0,0,1}, 
		  {13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5},
		  {13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5},
		  {13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5},
		  {13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5},
		  {13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5},
		  {13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5},
		  {13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5},
		  {13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5},
		  {13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5},
		  {13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5},
		  {13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5,13, 1, 4, 1,5},
		  };*/

	private static int[][] enemyWaveList = new int[100][20];
	private static final int baseNumOfCreep = 8;
	private static final int creepNumRate = 4;
	private static final float [][] probPerLevel = {
		{0},
		{1}, //level 1
		{0.9f, 0.1f},//level 2
		{0.8f, 0.2f},//level 3
		{0.7f, 0.3f},//level 4
		{0.6f, 0.3f, 0.1f},//level 5
		{0.5f, 0.3f, 0.2f},//level 6
		{0.4f, 0.3f, 0.2f, 0.1f},//level 7
		{0.3f, 0.3f, 0.2f, 0.1f, 0.1f},//level 8
		{0.2f, 0.2f, 0.2f, 0.2f, 0.2f},//level 9
		{0f}
	};
	
	static{
		int size = 0;
		int shift = 0;
		outer:
		for(int i = 1; i <= enemyWaveList.length; i++){
			int creepSize = 0;
			
			if(i > 1 && i%10 == 0){
				creepSize = 1; //per 10
			}else if( i == 1 ){
				creepSize = baseNumOfCreep;
			}else{
				creepSize = baseNumOfCreep + creepNumRate *(i%10);
			}
			
			size = probPerLevel[i%10].length;
			
			if(i != enemyWaveList.length){
				for(int j = 0; j < enemyWaveList[0].length; j++){
					int temp = 0;
					
					if(size > 0 && j >= shift){
						temp = (int) (creepSize * probPerLevel[i%10][j-shift]);
						
						size --;
						enemyWaveList[i][j] = temp;
					}else{
						enemyWaveList[i][j] = temp;
					}
				
				}
			}
			if(i%10 == 0 && i!= 0){
				if(i > 70)
					shift += 1;
				else
					shift += 2;
			}
		}
	}
	private List<AbstractCreep> enemyList;
	private static final String[] CREEP_LITERAL= {"C1", "C2", "C3", "C4", "C5", "C6", 
														"C7", "C8", "C9", "C10", "C11", "C12",
															  "C13","C14", "C15", "C16", "C17", "C18", 
															  		"C19", "C20"};
	public EnemyWave(){
		enemyList = new ArrayList<AbstractCreep>();//primary
	}
	
	public void setEnemyList(int waveNumber){
		for(int x = 0; x < enemyWaveList[waveNumber].length; x ++){
			for(int y = 0; y < enemyWaveList[waveNumber][x]; y ++){
				enemyList.add(CreepFactory.getInstance().getTestTarget(CREEP_LITERAL[x])); //get specified creep[x]
			}
		}
	}
	
	public List<AbstractCreep> getEnemyList(){
		System.out.println("Enemy List Size" + enemyList.size());
		try{
			Collections.shuffle(enemyList);
		}catch(Exception e){
			
		}
		return enemyList;
	}
	
	public void clearEnemyList(){
		enemyList.clear();
	}
	
	public int getNumberOfWaves(){
		return enemyWaveList.length;
	}
}
