package com.mygdx.handler;

public interface ResearchUIHandler {

	public void upgrade();
	public void evolveToOne();
	public void evolveToTwo();
	
}
