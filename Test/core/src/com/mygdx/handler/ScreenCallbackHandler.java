package com.mygdx.handler;

import com.mygdx.player.MyPlayer;

public interface ScreenCallbackHandler {

	public void selectScreen(int screenId, int toScreenId, MyPlayer player);
	public void openLeaderBoard();
	public void connectToGooglePlayServices();
	public boolean isSignedIn();
	public int getHighestScore();
	
}
