package com.mygdx.handler;

import com.mygdx.player.MyPlayer;
import com.mygdx.utils.Constants;

public interface AndroidCallbackHandler {
	
	public void callBack(float volumeSet);
	public MyPlayer loadFromLocalGame(MyPlayer player, String fileName);
	public void saveToLocalGame(MyPlayer player, String fileName);
}
