package com.mygdx.handler;

public interface GoogleServicesHandler {

	public void signIn();
	public void signOut();
	public void rateGame();
	public void submitScore(int score);
	public void showScore();
	public boolean isSignedIn();
	public void showSavedGamesUI();
	public int getHighestScore();
}
