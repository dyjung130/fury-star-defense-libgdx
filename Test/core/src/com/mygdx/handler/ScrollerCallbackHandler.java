package com.mygdx.handler;

public interface ScrollerCallbackHandler {

	public void callMoveBy(float deltaPosX, float deltaPosY);
}
