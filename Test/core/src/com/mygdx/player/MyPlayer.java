package com.mygdx.player;

import java.util.HashMap;
import java.util.Map;

import com.mygdx.utils.Constants;

public class MyPlayer {

	private static Map<String, Integer> playerProperties = new HashMap<String, Integer>();
	private static Map<String, Integer> playerResearchHM = new HashMap<String, Integer>();
	private static Map<String, Boolean> playerOptionHM = new HashMap<String, Boolean>();
	
	private static MyPlayer instance;
	
	private MyPlayer(){
		//singleton pattern
	}
	
	public static synchronized MyPlayer getInstance(){
		if(instance == null){
			instance = new MyPlayer();
			initialize();
		}
		
		return instance;
	}
	
	
	private static void initialize(){
		//player properties
		playerProperties.put(Constants.pL, new Integer(0));
		playerProperties.put(Constants.pE, new Integer(0));
		playerProperties.put(Constants.pO, new Integer(0));
		playerProperties.put(Constants.pB, new Integer(0));
		playerProperties.put(Constants.pI, new Integer(0));
		
		//level of each research 
		playerResearchHM.put(Constants.rH, new Integer(0));
		playerResearchHM.put(Constants.rD, new Integer(0));
		playerResearchHM.put(Constants.rF, new Integer(0));
		playerResearchHM.put(Constants.rG, new Integer(0));
		playerResearchHM.put(Constants.rS, new Integer(0));
		playerResearchHM.put(Constants.rC, new Integer(0));
		
		//player option 
		playerOptionHM.put(Constants.pOL, false);
		playerOptionHM.put(Constants.pOA, false);
		playerOptionHM.put(Constants.pOB, true);
		playerOptionHM.put(Constants.pOE,true);
	}
	
	/*public void updatePropertiesFromSavedGame(){
		//player properties
				playerProperties.put("level", new Integer(0));
				playerProperties.put("exp", new Integer(0));
				playerProperties.put("orbs", new Integer(0));
				playerProperties.put("bestScore", new Integer(0));
				playerProperties.put("inGameScore", new Integer(0));
				
				//level of each research 
				playerResearchHM.put("healthLvl", new Integer(0));
				playerResearchHM.put("damageLvl", new Integer(0));
				playerResearchHM.put("fireRateLvl", new Integer(0));
				playerResearchHM.put("goldLvl", new Integer(0));
				playerResearchHM.put("scoreLvl", new Integer(0));
				
				//player option 
				playerOptionHM.put("isLoggedIn", false);
				playerOptionHM.put("isAutoLoginEnabled", false);
				playerOptionHM.put("isBgmEnabled", true);
				playerOptionHM.put("isEffectEnabled",true);
	}*/
	
	public Map<String, Integer> getPropertyMap(){
		return playerProperties;
	}
	
	public void setPropertyMap(Map<String, Integer> map){
		
	}
	
	
	public Map<String, Integer> getResearchMap(){
		return playerResearchHM;
	}

	public void setResearchMap(Map<String, Integer> map){
		playerResearchHM.put("healthLvl", map.get("healthLvl"));
		playerResearchHM.put("damageLvl", map.get("damageLvl"));
		playerResearchHM.put("fireRateLvl", map.get("fireRateLvl"));
		playerResearchHM.put("goldLvl", map.get("goldLvl"));
		playerResearchHM.put("scoreLvl", map.get("scoreLvl"));
		playerResearchHM.put("coolDownLvl", map.get("coolDownLvl"));
	}
	
	
	public Map<String, Boolean> getOptionMap(){
		return playerOptionHM;
	}
	
	public void setOptionMap(Map<String, Boolean> map){
		
	}
	
	
	/*
	//global variables form Google Drive (Saved Game) or from preference depending on login status
	private static int userLevel = 1, userTotalExp = 0, userMaxScore = 0, userAccScore = 0, userCurrentOrbs = 0;
	//level of each global research area [base health, target dmg, target firerate, moneycollector, gameSpeed button upgrade,
	private static int[] userResearches = {1,1,1,1,1,1};
	private static HashMap userResearchHM = new HashMap();
	//local file variables from preferences
	//private static boolean isLoggedIn = false, isAutoLoginEnabled = false, isBgmEnabled = true, isEffectDisabled = false;
	private static boolean[] userOptions = {false, false, true, false};
	
	//in game variables (money = build tower, score = submittable score, ingameorbs for ultimate upgrade)
	private int inGameMoney = 500, inGameCurrentScore = 0, inGameOrbs = 0;
	//level of each in game research area
	private int[] inGameResearch = {1,1,1,1,1,1};
	
	private static MyPlayer playerInstance;
	
	private MyPlayer(){
		//singleton pattern

	}
	
	public static synchronized MyPlayer getInstance(){
		if(playerInstance == null){
		 playerInstance = new MyPlayer();
		}
	
		return playerInstance;
	}
	
	public void setProperties(int userLevel, int userTotalExp, int userAccScore, int userCurrentOrbs,
												int[] userResearches, boolean[] userOptions){
		
		if(playerInstance != null){
			
			MyPlayer.userLevel = userLevel;
			MyPlayer.userTotalExp = userTotalExp;
			MyPlayer.userAccScore = userAccScore;
			MyPlayer.userCurrentOrbs = userCurrentOrbs;
			MyPlayer.userResearches = userResearches;
			MyPlayer.userOptions = userOptions;
			
		}
		
	}
	
	public int getLevel(){
		return userLevel;
	}
	
	public void setLevel(int level){
		userLevel = level;
	}
	
	public  int getTotalExp(){
		return userTotalExp;
	}
	
	public  void addToTotalExp(int exp){
		userTotalExp += exp;
	}
	
	public  int getAccScore(){
		return userAccScore;
	}
	
	public  void addToAccScore(int score){
		userAccScore += score;
	}
	
	public  int getCurrentOrbs(){
		return userCurrentOrbs;
	}
	
	public  void addToCurrentOrbs(int orbs){
		userCurrentOrbs += orbs;
	}
	
	public  int[] getResearches(){
		return userResearches;
	}
	
	public  void upgradeResearches(int orbs, int index){
		userResearches[index] = orbs;
	}
	
	public  boolean[] getOptions(){
		return userOptions;
	}
	
	public  void setOptions(boolean b, int index){
		userOptions[index] = b;
	}
	*/
}
