package com.mygdx.userinterface;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mygdx.actors.AbstractTowerArea;
import com.mygdx.actors.BitmapFontActor;
import com.mygdx.actors.AbstractTowerMulti;
import com.mygdx.actors.AbstractTowerSingle;
import com.mygdx.actors.AbstractTowerUtil;
import com.mygdx.stage.BottomStage;
import com.mygdx.stage.TopStage;
import com.mygdx.towers.*;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;
import com.mygdx.utils.TowerType;

public class ResearchUI{
	
	private static ResearchUI instance;
	private Image  uTower, eTowerOne, eTowerTwo, uBox, eBox1, eBox2;
	private Image bottomUI;
	
	private BitmapFontActor attk, rng, eff, cost, level, upgradeBitmapFont, evolveBitmapFont, costBitmapFont;
	private Texture textureOne, textureTwo;
	private Actor actor;
	private byte currentTowerID;
	private int evolveCost = Constants.evolveCost;
	private ParticleEffect particleEffect = new ParticleEffect();
	//initialize variables
	
/*		eBg = new Image(Assets.getAssetManager().get("UtilityImage/smallBox.png", Texture.class));
		eBg.setPosition(0, Constants.P_SCROLLBAR + Constants.UI_TOWERGAP*2);
		eBg.setSize(Constants.APP_WIDTH - Constants.UI_TOWERLISTX, Constants.UI_TOWERLISTY2);
		eBg.setColor(Color.BLACK);
		//right side ui
		uBg = new Image(Assets.getAssetManager().get("UtilityImage/smallBox.png", Texture.class));
		uBg.setPosition(Constants.UI_TOWERLISTX, Constants.P_SCROLLBAR + Constants.UI_TOWERGAP*2);
		uBg.setSize(Constants.APP_WIDTH - Constants.UI_TOWERLISTX, Constants.UI_TOWERLISTY2);

		//tower image background + image (1)
		eFgOne= new Image(Assets.getAssetManager().get("UtilityImage/smallBox.png", Texture.class));
		eFgOne.setColor(Color.RED);
		eFgOne.setBounds(10, Constants.P_SCROLLBAR + Constants.UI_TOWERGAP*2 + 10, 
				Constants.APP_WIDTH - Constants.UI_TOWERLISTX - 20, (Constants.UI_TOWERLISTY2/2)-20);
		//tower image background + image (1)
				eFgTwo= new Image(Assets.getAssetManager().get("UtilityImage/smallBox.png", Texture.class));
				eFgTwo.setColor(Color.BLUE);
				eFgTwo.setBounds(10, Constants.P_SCROLLBAR + Constants.UI_TOWERGAP*2 + 10 + (Constants.UI_TOWERLISTY2 - 20)/2, 
						Constants.APP_WIDTH - Constants.UI_TOWERLISTX - 20, (Constants.UI_TOWERLISTY2/2)-20);
		*/		
	{			
		eBox1 = new Image(Assets.getAssetManager().get("UtilityImage/smallBox.png", Texture.class));
		eBox1.setBounds(35, Constants.UI_TOWERGAP*2 + 15, 110, 110);
		eBox1.setColor(Color.CYAN);
		eTowerOne = new Image(Assets.getAssetManager().get("UtilityImage/smallBoxNo.png", Texture.class));
		//eTowerOne.setColor(Color.RED);
		eTowerOne.setBounds(35, Constants.UI_TOWERGAP*2 + 15, 110, 110);
		evolveBitmapFont = new BitmapFontActor(35, Constants.P_SCROLLBAR - 10  + (Constants.UI_TOWERLISTY2 - 50)/2, 
				Constants.TOWER_DIM-25, Constants.TOWER_DIM-25, "|EVOLVE|");
		costBitmapFont = new BitmapFontActor(35, Constants.P_SCROLLBAR - 24  + (Constants.UI_TOWERLISTY2 - 50)/2, 
				Constants.TOWER_DIM-25, Constants.TOWER_DIM-25, evolveCost, "C | ");
		
		eBox2 = new Image(Assets.getAssetManager().get("UtilityImage/smallBox.png", Texture.class));
		eBox2.setBounds(35, Constants.P_SCROLLBAR - 10  + (Constants.UI_TOWERLISTY2 - 50)/2, 
				110, 110);
		eBox2.setColor(Color.CYAN);
		
		eTowerTwo = new Image(Assets.getAssetManager().get("UtilityImage/smallBoxNo.png", Texture.class));
		//eTowerTwo.setColor(Color.RED);
		eTowerTwo.setBounds(35, Constants.P_SCROLLBAR - 10  + (Constants.UI_TOWERLISTY2 - 50)/2, 
				110, 110);
		//tower images
	
		uBox =  new Image(Assets.getAssetManager().get("UtilityImage/smallBox.png", Texture.class));
		uBox.setBounds(Constants.UI_TOWERLISTX +12, Constants.UI_TOWERGAP*24, 
				120,120);
		uBox.setColor(Color.CYAN);
		
		uTower = new Image(Assets.getAssetManager().get("UtilityImage/smallBox.png", Texture.class));
	//	uTower.setColor(Color.YELLOW);
		uTower.setBounds(Constants.UI_TOWERLISTX +12, Constants.UI_TOWERGAP*24, 
				120,120);
	//	uTower.setPosition(Constasnts.UI_TOWERLISTX + 50, Constants.P_SCROLLBAR + Constants.UI_TOWERGAP*2 + 90);
	//	uTower.setSize(Constants.APP_WIDTH - Constants.UI_TOWERLISTX - 100, Constants.UI_TOWERLISTY2 - 100);
	
		//initialize bitmapfont actors
		upgradeBitmapFont = new BitmapFontActor(Constants.UI_TOWERLISTX + Constants.UI_TOWERGAP, Constants.UI_TOWERGAP*22, 
				Constants.TOWER_DIM-25, Constants.TOWER_DIM-25, "|UPGRADE|");
		level = new BitmapFontActor(Constants.UI_TOWERLISTX + 10, Constants.UI_TOWERGAP*19, 80, 20, 0, "LV | ");
		cost = new BitmapFontActor(Constants.UI_TOWERLISTX + 10, Constants.UI_TOWERGAP*16, 80, 20, 0, "C | ");
		attk = new BitmapFontActor(Constants.UI_TOWERLISTX + 10,Constants.UI_TOWERGAP*13, 80,20, 0, "P | ");
		rng = new BitmapFontActor(Constants.UI_TOWERLISTX + 10, Constants.UI_TOWERGAP*10, 80, 20, 0, "R | ");
		eff = new BitmapFontActor(Constants.UI_TOWERLISTX + 10, Constants.UI_TOWERGAP*7, 80, 20, "EFFECT");
		
		bottomUI= new Image(Assets.getAssetManager().get("BackgroundImage/bottomUI.png", Texture.class));
		bottomUI.setBounds(0,  0, Constants.APP_WIDTH, Constants.UI_TOWERLISTY2 * 6/4);
	
		//particle effect
		particleEffect.load(Gdx.files.internal("Effect/researchEffect.p"),Gdx.files.internal("Effect"));
	//	particleEffect.scaleEffect(0.5f);
		particleEffect.scaleEffect(2f);
		particleEffect.getEmitters().first().setPosition(Gdx.graphics.getWidth()/2, 0);
		particleEffect.setPosition(Gdx.graphics.getWidth()/2, 0);
		particleEffect.start();
		
	}
	
	private ResearchUI(Stage stage){
		//Singleton Pattern
		addAllActors(stage);
		addListeners();
	}
	
	private void addListeners(){
		eTowerOne.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				if(TopStage.getInstance().getMoney() >= evolveCost){
					switch(currentTowerID){
					case Constants.SINGLE_1:
						((TowerSingle)actor).evolveToOne();
						TopStage.getInstance().updateMoney(evolveCost);
						drawParticleEffect();
						break;
					case Constants.MULTI_1:
						((TowerMulti)actor).evolveToOne();
						TopStage.getInstance().updateMoney(evolveCost);
						drawParticleEffect();
						break;
					case Constants.AREA_1:
						((TowerArea)actor).evolveToOne();
						TopStage.getInstance().updateMoney(evolveCost);
						drawParticleEffect();
						break;
					case Constants.UTIL_1:
						((TowerUtil)actor).evolveToOne();
						TopStage.getInstance().updateMoney(evolveCost);
						drawParticleEffect();
						break;
					default:
						break;
					}
					reinitialize();
				}
			
				return false;
			}
		});
		
		eTowerTwo.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				if(TopStage.getInstance().getMoney() >= evolveCost){
					switch(currentTowerID){
					case Constants.SINGLE_1:
						((TowerSingle)actor).evolveToTwo();
						TopStage.getInstance().updateMoney(evolveCost);
						drawParticleEffect();
						break;
					case Constants.MULTI_1:
						((TowerMulti)actor).evolveToTwo();
						TopStage.getInstance().updateMoney(evolveCost);
						drawParticleEffect();
						break;
					case Constants.AREA_1:
						((TowerArea)actor).evolveToTwo();
						TopStage.getInstance().updateMoney(evolveCost);
						drawParticleEffect();
						break;
					case Constants.UTIL_1:
						((TowerUtil)actor).evolveToTwo();
						TopStage.getInstance().updateMoney(evolveCost);
						drawParticleEffect();
						break;
					default:
						break;
					}
					reinitialize();
				}

				return false;
			}
		});
		
		uTower.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				
				//for instanceof null check is never needed 03222015
				if(actor instanceof AbstractTowerSingle){
					((AbstractTowerSingle) actor).upgrade();
					drawParticleEffect();
					System.out.println("ST successfully upgraded");
				}
				
				if(actor instanceof AbstractTowerMulti){
					((AbstractTowerMulti) actor).upgrade();
					drawParticleEffect();
					System.out.println("MT successfully upgraded");
				}
				
				if(actor instanceof AbstractTowerArea){
					((AbstractTowerArea) actor).upgrade();
					drawParticleEffect();
					System.out.println("AT successfully upgraded");
				}
				
				if(actor instanceof AbstractTowerUtil){
					((AbstractTowerUtil) actor).upgrade();
					drawParticleEffect();
					System.out.println("UT successfully upgraded");
				}
			
				return false;
			}
		});
		
	}
	
	private void drawParticleEffect(){
		
		if(!particleEffect.isComplete()){
			particleEffect.reset();
		}
		particleEffect.start();
		TopStage.getInstance().setParticleEffect(particleEffect);
	}
	
	//instance getter
	public static synchronized ResearchUI getInstance(Stage stage){
		if(instance == null){
			instance = new ResearchUI(stage);
		}
		
		return instance;
	}
	
	public static synchronized ResearchUI getInstance(){
		//needs to check if valid
		return instance;
	}
	

	public void setUpgradeProperties(Texture texture, int attkVal, int rngVal, int levelVal, int effVal, int costVal, String effectName){
		uTower.setDrawable((new SpriteDrawable(new Sprite(texture))));
		
		attk.updateValue(attkVal);
		rng.updateValue(rngVal);
		level.updateValue(levelVal);
		eff.updateName2(effectName);
		cost.updateValue(costVal);
	}
	
	private void check(byte towerID){
		switch(towerID){
		case Constants.SINGLE_1:
			textureOne = TowerType.SINGLE1E.getTexture();
			textureTwo =TowerType.SINGLE2E.getTexture();
			break;
		case Constants.MULTI_1:
			textureOne = TowerType.MULTI1E.getTexture();
			textureTwo = TowerType.MULTI2E.getTexture();
			break;
		case Constants.AREA_1:
			textureOne = TowerType.AREA1E.getTexture();
			textureTwo = TowerType.AREA2E.getTexture();
			break;
		case Constants.UTIL_1:
			textureOne = TowerType.UTIL1E.getTexture();
			textureTwo = TowerType.UTIL2E.getTexture();
			break;
		default:
			System.out.println("evolve done");
			textureOne = Assets.getAssetManager().get("UtilityImage/smallBox.png", Texture.class);
			textureTwo = Assets.getAssetManager().get("UtilityImage/smallBox.png", Texture.class);
			break;
		}
	}
	
	public void setEvolveProperties(Actor actor,byte towerID){
		check(towerID);
		this.actor = actor;
		this.currentTowerID = towerID;
		eTowerOne.setDrawable((new SpriteDrawable(new Sprite(textureOne))));
		
		eTowerTwo.setDrawable((new SpriteDrawable(new Sprite(textureTwo))));
		
		/*if(actor instanceof SingleTargetTower){
			eFgOne.setDrawable((new SpriteDrawable(new Sprite(texture))));
			eFgTwo.setDrawable((new SpriteDrawable(new Sprite(texture))));
			
			eTowerOne.setDrawable((new SpriteDrawable(new Sprite(textureOne))));
			eTowerTwo.setDrawable((new SpriteDrawable(new Sprite(textureTwo))));
			
		}
		
		if(actor instanceof MultiTargetTower){
			
		}
		
		if(actor instanceof AreaActor){
			
		}*/
	}
	
	//add all actors associated with
	private void addAllActors(Stage stage){
		try{
		
			stage.addActor(bottomUI);
			//background images
		//	stage.addActor(eBg);	stage.addActor(uBg);
			//foreground images
		//	stage.addActor(eFgOne);	stage.addActor(eFgTwo);//evolve option one & two
	
			//surrounding boxes
			stage.addActor(eBox1);
			stage.addActor(eBox2);
			stage.addActor(uBox);
			
			//tower images 
			stage.addActor(eTowerTwo);
			stage.addActor(eTowerOne);
			// upgrade assets
			stage.addActor(upgradeBitmapFont);
			stage.addActor(uTower);//upgrade this tower
			
			//property images
			
			stage.addActor(attk); stage.addActor(rng); stage.addActor(level); stage.addActor(eff); stage.addActor(cost);
			stage.addActor(costBitmapFont); stage.addActor(evolveBitmapFont);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("Error in addAllActors wrong stage");
		}
	}
	
	public void clearAll(){
		instance = null;
		particleEffect.dispose();
		bottomUI.remove();
		//eBg.remove(); uBg.remove(); eFgOne.remove(); eFgTwo.remove();
		eTowerOne.remove(); eTowerTwo.remove();
		uTower.remove();
		//remove boxes
		eBox1.remove(); eBox2.remove(); uBox.remove();
		//remove stats
		upgradeBitmapFont.remove();
		attk.remove(); rng.remove(); level.remove(); eff.remove(); cost.remove();
		evolveBitmapFont.remove(); costBitmapFont.remove();
	}

	/*
	 * reinitialize must be called when evolves. possibly for upgrade as well.
	 */
	public void reinitialize(){
		currentTowerID = 0;
		textureOne = Assets.getAssetManager().get("UtilityImage/smallBoxNo.png", Texture.class);
		textureTwo =Assets.getAssetManager().get("UtilityImage/smallBoxNo.png", Texture.class);
		eTowerOne.setDrawable((new SpriteDrawable(new Sprite(textureOne))));
		eTowerTwo.setDrawable((new SpriteDrawable(new Sprite(textureTwo))));
		
	
	}
	
}
