package com.mygdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.denden130.furystardefense.Main;
import com.mygdx.player.MyPlayer;
import com.mygdx.stage.BottomStage;
import com.mygdx.stage.OptionStage;
import com.mygdx.stage.ProjectileStage;
import com.mygdx.stage.TopStage;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;

/*
 * PlayScreen.java
 * 1) Handles Game Stages and Option Stage
 * 2) It also handles game: gameover, backtomenu, & submit score
 */
public class PlayScreen implements Screen{
	
	final Main main;
	//TestStage --> draws background, targets, and etc.
	private BottomStage botStage;
	//TopStage  --> UI, {Actor(Towers), not working well..} 
	private TopStage topStage;
	//ProjectilStage --> draws bullet on proper trajectory
	private ProjectileStage proStage;
	//OptionStage
	private OptionStage optionStage;
	private InputMultiplexer inputMulti;
	private boolean backPressed, screenTabbed;
	private Image backButton;
	private MyPlayer player;
	//Assets 
	private final Skin skin =  Assets.getAssetManager().get("Skin/uiskin.json", Skin.class);
	private Label gameOverMessage= new Label("GAME OVER", skin);
	private Label gameCompleteMessage = new Label("CONGRATULATION!!", skin);
	private Label backToMenuMessage = new Label("BACK TO THE MAIN MENU?", skin);
	private Label tab2Start;
	private Music bgMusic = Gdx.audio.newMusic(Gdx.files.internal("Music/bgPlayScreen.ogg"));//background music
	
	//dialog
	private int dialogResult = 0;//just initialize with 0
	//ensures that dialog opens only once (if multiple dialog is added, it will lag bad)
	private boolean dialogIsOpen = false;
	
	
	public PlayScreen(final Main main, MyPlayer player){
		super();
		this.main = main;
		this.player = player;
		this.botStage = BottomStage.getStageInstance(main);//Bottom Stage
		this.topStage = TopStage.getTopStageInstance(main, player);//UI (top) stage 
		this.proStage = ProjectileStage.getInstance();//projectile stage (to avoid overlap of images)
		this.optionStage = OptionStage.getInstance(main);//option stage
		callBackButton();
		//Input Multiplexer for each stages
		inputMulti = new InputMultiplexer();
		inputMulti.addProcessor(optionStage);//must be added first (this is the very first layer for touch input)
		inputMulti.addProcessor(botStage);
		inputMulti.addProcessor(topStage);
		
		
		//this is the button where the user will tab and be able to start game
		tab2Start = new Label("TAB HERE TO START!", skin);
		tab2Start.setPosition(Constants.APP_WIDTH/2 - 150, Constants.APP_HEIGHT/2 - 50);
		tab2Start.setSize(350, 100);
		tab2Start.setFontScale(2f);
		tab2Start.addAction(Actions.forever(Actions.sequence(Actions.fadeIn(0.4f), Actions.fadeOut(0.6f))));
		tab2Start.setColor(Color.WHITE);
		tab2Start.setWrap(true);
		tab2Start.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				tab2Start.setColor(Color.GRAY);
				screenTabbed = true;
				return false;
			}
			
		});
		
		Gdx.input.setInputProcessor(inputMulti);
		Gdx.input.setCatchBackKey(true);//exit the app on back key pressed

	}
	
	private void callBackButton(){
		backButton = new Image(Assets.getAssetManager().get("ButtonImage/menuIcon.png", Texture.class));
		backButton.setBounds(23, Constants.APP_HEIGHT - 100, 100, 70);
		backButton.addListener(new ClickListener(){
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				backPressed = true;
				
				return true;
			}
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				super.touchUp(event, x, y, pointer, button);
				if(dialogIsOpen == false){
					showDialog(backToMenuMessage);
					dialogIsOpen = true;
				}
				//dispose();
				//main.selectScreen(Constants.loadingScreen, Constants.mainScreen, player);
				
			}
		});
		
		topStage.addActor(backButton);
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
	
	}

	@Override
	public void render(float delta) {
		bgMusic.play();
		
		//Gdx to avoid the trace created from the texture
		Gdx.gl.glClearColor(1, 1, 1, 1);
		//important to put Gdx.gl.glClearColor in Screen
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA,GL20.GL_ONE_MINUS_SRC_ALPHA);
		//start draw
	
		if(!(botStage.getGameOverStatus() || botStage.getGameCompleteStatus())){
		//Game Stage
			//bottom stage
			botStage.draw();
			//botStage.act();
			//top stage
			topStage.draw();
			//topStage.act();
			//proj stage
			proStage.draw();
			//proStage.act();
			
		//start act when the user tabs on screen
			if(screenTabbed){
				if(optionStage.getActors().contains(tab2Start, false)){
					optionStage.getActors().pop();
				}	
				botStage.act();
				topStage.act();
				proStage.act();
			}else{
				optionStage.addActor(tab2Start);
				
			}
			
		}else if(botStage.getGameOverStatus()){	
			//disposeGameStages();
		

			//bottom stage
			botStage.draw();
			//botStage.act();
			//top stage
			topStage.draw();
			//topStage.act();
			//proj stage
			proStage.draw();
			//proStage.act();
			//set false so that the user can see tap screen to start button later
			screenTabbed = false;
			
			if(dialogIsOpen == false){

				
				showDialog(gameOverMessage);//shows a dialog message if the user wishes to retry or quit
				dialogIsOpen = true;
			}
		
		}else if(botStage.getGameCompleteStatus()){

			//bottom stage
			botStage.draw();
			//botStage.act();
			//top stage
			topStage.draw();
			//topStage.act();
			//proj stage
			proStage.draw();
			//proStage.act();
			//set false so that the user can see tap screen to start button later
			screenTabbed = false;
			
			if(dialogIsOpen == false){
				showDialog(gameCompleteMessage);//shows a dialog message if the user wishes to retry or quit
				dialogIsOpen = true;
			}
		}
		
		//option stage
		optionStage.draw();
		optionStage.act();
		
		Gdx.gl.glDisable(GL20.GL_BLEND);
		
		
	}


	
	//call dialog
	private void showDialog(Label message){
		
		message.setWrap(true); // prevents overflowing of the letters in the dialog box
		message.setFontScale(1.5f);
		message.setColor(Color.RED);
		if(message.equals(gameCompleteMessage)){
			message.setColor(Color.YELLOW);
		}else if(message.equals(backToMenuMessage)){
			message.setColor(Color.CYAN);
		}
		message.setAlignment(Align.center);
		
		Dialog dialog = new Dialog("", skin, "default"){
			//option displayed and game flow 
			@Override
			protected void result(Object object){
				if(object instanceof Integer){
					dialogResult = (Integer) object;
					
					
					switch(dialogResult){
						case Constants.GOTOMENU:
							//dispose this screen's stages first in order to make sure the stages instances are reset
							disposeGameStages();
							main.selectScreen(Constants.playScreen, Constants.mainScreen, player);
							break;
						case Constants.REPLAY:
							//dispose this screen's stages first in order to make sure the stages instances are reset
							disposeGameStages();
							main.selectScreen(Constants.playScreen, Constants.playScreen, player);
							break;
						case Constants.SUBMITSCORE:
							callSubmitScore();
							break;
						default:
							break;
					}
				}
				if(object instanceof Boolean){
					dialogIsOpen = (Boolean) object;
				}
			}
		};
		
		dialog.padTop(30f);
		dialog.getContentTable().add(message).width(300).row();
		dialog.getButtonTable().padTop(50f).defaults().height(80).width(300);
		//dialog.addActor(image);
		
		TextButton replayButton = new TextButton("REPLAY", skin, "toggle");
		TextButton menuButton = new TextButton("BACK TO MENU", skin, "toggle");
		TextButton submitButton = new TextButton("SUBMIT SCORE", skin, "toggle");
		TextButton noButton = new TextButton("CONTINUE GAME", skin, "toggle");
		
		if(message.equals(gameOverMessage)){
			//if game is over, show replay
			dialog.button(replayButton, Constants.REPLAY);
		}
		
		if(message.equals(gameOverMessage) || message.equals(gameCompleteMessage)){
			//if game is over or game is completed
			dialog.getButtonTable().row();
			dialog.getButtonTable().row();
			dialog.button(menuButton, Constants.GOTOMENU).row();
			dialog.getButtonTable().row();
			dialog.getButtonTable().row();
			dialog.button(submitButton, Constants.SUBMITSCORE);
		}
		
		if(message.equals(backToMenuMessage)){
			//if backbutton is pressed (main menu button)
			dialog.getButtonTable().row();
			dialog.button(menuButton, Constants.GOTOMENU).row();
			dialog.getButtonTable().row();
			dialog.getButtonTable().row();
			dialog.button(noButton, false);
		}
		
		//dialog.key(Keys.ENTER, true).key(Keys.ESCAPE, false);
		dialog.invalidateHierarchy();
		dialog.invalidate();
		dialog.layout();
	
		dialog.show(optionStage);
	}
	

	
	//on click submit score in the dialog options, check if the user is logged in and prompt to either submitscore-> lb or login
	private void callSubmitScore(){
		if(!(main.isSignedIn())){
			try{
				main.gsh.signIn();
				
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(main.isSignedIn())
					main.submitScore(topStage.getScore());
				//render the dialog box 
				dialogIsOpen = false;
			}
		}else{
			//show sign in button 
			main.submitScore(topStage.getScore());
			dialogIsOpen = false;
		}
	}
	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		
	
		//keep the option stages shown on the screen
		
		//dispose all game stages currently viewed in the stage
		disposeGameStages();
	}

	private void disposeGameStages(){
		bgMusic.stop();
		bgMusic.dispose();
		optionStage.clear();
		botStage.clear();
		proStage.clear();
		topStage.clear();
		
		optionStage.dispose();
		botStage.dispose();
		proStage.dispose();
		topStage.dispose();
	}


}
