package com.mygdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.denden130.furystardefense.Main;
import com.mygdx.handler.ScreenCallbackHandler;
import com.mygdx.player.MyPlayer;
import com.mygdx.stage.LoadingStage;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;

public class LoadingScreen implements Screen{

	private LoadingStage stage;
	private int toScreenId;
	private float totalTime, loadingTime = 1.5f;
	private ScreenCallbackHandler scbh;
	private MyPlayer player;
	
	public LoadingScreen(final Main main, MyPlayer player, int toScreenId){
		super();
		stage = new LoadingStage(main);
		scbh = main;
		
		this.player = player;
		this.toScreenId = toScreenId;
		loadAsset();
		Gdx.input.setCatchBackKey(false);//exit the app on back key pressed
		
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}
	private void loadAsset(){
		Assets.getAssetManager();
		Assets.load();
	
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub

		//Gdx to avoid the trace created from the texture
		Gdx.gl.glClearColor(0, 0, 0, 0);
		//important to put Gdx.gl.glClearColor in Screen
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA,GL20.GL_ONE_MINUS_SRC_ALPHA);
		//start draw
		if(!Assets.getAssetManager().update()){
			stage.updateTime((byte) (Assets.getAssetManager().getProgress() * 100 ));
		
		}else{
			try{
				Assets.getAssetManager().finishLoading();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				stage.addLoadingActions();
				
			}
			
			stage.switchToFlashScreen();		
		}
			//Assets.getAssetManager().finishLoading();
		
	
	
		stage.act(delta);
		stage.draw();
		
		if(stage.flashScreenEnded()){
			scbh.selectScreen(Constants.loadingScreen, toScreenId, player);
		}

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
	}

}
