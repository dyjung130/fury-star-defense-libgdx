package com.mygdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.denden130.furystardefense.Main;
import com.mygdx.handler.StageCallbackHandler;
import com.mygdx.player.MyPlayer;
import com.mygdx.stage.MainMenuStage;
import com.mygdx.stage.OptionStage;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;

public class MainScreen implements Screen{

	
	private MainMenuStage stage;
	//private OptionStage oStage; 
	private final Main main;
	private InputMultiplexer inputMulti;
	private boolean optionCalled;
	
	public MainScreen(final Main main, MyPlayer player){
		super();
		this.main = main;
		stage = MainMenuStage.getInstance(main, player);
	//	oStage = OptionStage.getInstance(main);
		inputMulti = new InputMultiplexer();
		inputMulti.addProcessor(stage);
		//inputMulti.addProcessor(oStage);
		Gdx.input.setInputProcessor(inputMulti);
		//Gdx.input.setCatchBackKey(false);//exit the app on back key pressed
		Gdx.input.setCatchBackKey(true);//exit the app on back key pressed

	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float delta) {
		
		// TODO Auto-generated method stub
		//Gdx to avoid the trace created from the texture
		Gdx.gl.glClearColor(1, 1, 1, 1);
		//important to put Gdx.gl.glClearColor in Screen
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA,GL20.GL_ONE_MINUS_SRC_ALPHA);
		//start draw
	
		stage.draw();
		stage.act();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	
		stage.dispose();
	//	oStage.dispose();
		
	}



}
