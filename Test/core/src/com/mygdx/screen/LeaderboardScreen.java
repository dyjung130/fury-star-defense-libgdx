package com.mygdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.denden130.furystardefense.Main;
import com.mygdx.player.MyPlayer;

public class LeaderboardScreen implements Screen{
	
	private Main main;
	
	public LeaderboardScreen(final Main main, MyPlayer player){
		super();
		this.main = main;

	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		main.openLeaderBoard();
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub

		//Gdx to avoid the trace created from the texture
		Gdx.gl.glClearColor(0, 0, 0, 0);
		//important to put Gdx.gl.glClearColor in Screen
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA,GL20.GL_ONE_MINUS_SRC_ALPHA);
		
		Gdx.gl.glDisable(GL20.GL_BLEND);
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
