package com.mygdx.utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class Assets{

	private static AssetManager assetManager;
	private static TextureParameter param = new TextureParameter();
	
	private Assets(){
		//singleton pattern
	}
	
	public static synchronized AssetManager getAssetManager(){
		if(assetManager == null){
			assetManager = new AssetManager(); 
			
		}
		return assetManager;
	}
	
	
	
	public static void load(){
		param.minFilter = TextureFilter.Linear;
		param.magFilter = TextureFilter.Linear;
	
		loadSkin();
		loadMainMenuImage();
		loadTargetImage();
		loadCreepTexture();
		loadTileImage();
		loadProjectileImage();
		loadUIImage();
		loadPlayUIImage();
		loadTowerImage();
		loadIcons();
		loadButtons();
		playScreenImages();
		
		
	}
	
	private static void loadSkin(){
		assetManager.load("Skin/uiskin.json", Skin.class);
		assetManager.load("Skin/hiero.fnt", BitmapFont.class); 
		assetManager.load("Skin/uiskin.atlas", TextureAtlas.class);
	}
	
	private static void loadTargetImage(){
		assetManager.load("badlogic.jpg", Texture.class);
		assetManager.load("health.jpg", Texture.class);
	}
	
	private static void loadCreepTexture(){
		//Creep Soul
		assetManager.load("monster/creepSoul.png", Texture.class, param);
		//Mecha Rabbit
		assetManager.load("monster/mecha1.png", Texture.class, param);
		assetManager.load("monster/mecha2.png", Texture.class, param);
		assetManager.load("monster/mecha3.png", Texture.class, param);
		assetManager.load("monster/mecha4.png", Texture.class, param);
		//Missiles
		assetManager.load("monster/missile1.png", Texture.class, param);
		assetManager.load("monster/missile2.png", Texture.class, param);
		assetManager.load("monster/missile3.png", Texture.class, param);
		assetManager.load("monster/missile4.png", Texture.class, param);
		assetManager.load("monster/missile5.png", Texture.class, param);
		//Robots
		assetManager.load("monster/robot1.png", Texture.class, param);
		assetManager.load("monster/robot2.png", Texture.class, param);
		assetManager.load("monster/robot3.png", Texture.class, param);
		assetManager.load("monster/robot4.png", Texture.class, param);
		assetManager.load("monster/robot5.png", Texture.class, param);
		//Shadow
		assetManager.load("monster/shadow1.png", Texture.class, param);
		assetManager.load("monster/shadow2.png", Texture.class, param);
		assetManager.load("monster/shadow3.png", Texture.class, param);
		assetManager.load("monster/shadow4.png", Texture.class, param);
		assetManager.load("monster/shadow5.png", Texture.class, param);
		assetManager.load("monster/shadow6.png", Texture.class, param);
	
	}
	
	private static void loadTowerImage(){
		//set images (04032015)
		/*single target*/
		assetManager.load("TowerImage/single1.png", Texture.class, param);
		assetManager.load("TowerImage/single2a.png", Texture.class, param);
		assetManager.load("TowerImage/single2b.png", Texture.class, param);
		/*multi target*/
		assetManager.load("TowerImage/multi1.png", Texture.class, param);
		assetManager.load("TowerImage/multi2a.png", Texture.class, param);
		assetManager.load("TowerImage/multi2b.png", Texture.class, param);
		/*area target*/
		assetManager.load("TowerImage/area1.png", Texture.class, param);
		assetManager.load("TowerImage/area2a.png", Texture.class, param);
		assetManager.load("TowerImage/area2b.png", Texture.class, param);
		/*util target*/
		assetManager.load("TowerImage/util1.png", Texture.class, param);
		assetManager.load("TowerImage/util2a.png", Texture.class, param);
		assetManager.load("TowerImage/util2b.png", Texture.class, param);
				
	}
	
	private static void loadTileImage(){
		assetManager.load("selectedTile.png", Texture.class, param);
		assetManager.load("tile.png", Texture.class, param);
		assetManager.load("tile.jpg", Texture.class, param);
		assetManager.load("path.png", Texture.class, param);
	}
	
	private static void loadProjectileImage(){
		assetManager.load("projectile.jpg", Texture.class);
		assetManager.load("projectile.png", Texture.class);
	}
	
	private static void loadUIImage(){
		assetManager.load("UtilityImage/towerPointer.png", Texture.class, param);
		assetManager.load("UtilityImage/loadingIcon.png", Texture.class, param);
		assetManager.load("UtilityImage/optionIcon.png", Texture.class, param);
		assetManager.load("UtilityImage/exitIcon.png", Texture.class, param);
		assetManager.load("UtilityImage/smallBox.png", Texture.class, param);
		assetManager.load("UtilityImage/smallBoxNo.png", Texture.class, param);
		
	}
	
	private static void loadPlayUIImage(){
		assetManager.load("BackgroundImage/topUI.png", Texture.class, param);
		assetManager.load("BackgroundImage/bottomUI.png", Texture.class, param);
	}
	
	private static void loadMainMenuImage(){
		assetManager.load("BackgroundImage/title.png", Texture.class, param);
		assetManager.load("BackgroundImage/fullbgtest.png", Texture.class, param);
	}
	
	private static void loadIcons(){
		
		//Game Play Screen UI Icons (040315)
		assetManager.load("Icons/gold.png", Texture.class, param);
		assetManager.load("Icons/health.png", Texture.class, param);
		assetManager.load("Icons/score.png", Texture.class, param);
		assetManager.load("Icons/time.png", Texture.class, param);
		assetManager.load("Icons/wave.png", Texture.class, param);
		
		//Game Play Unit Icon
		assetManager.load("Icons/unitUtil.png", Texture.class, param);
		assetManager.load("Icons/unitArea.png", Texture.class, param);
		assetManager.load("Icons/unitSingle.png", Texture.class, param);
		assetManager.load("Icons/unitMulti.png", Texture.class, param);
		assetManager.load("Icons/pathArrow.png", Texture.class, param);
	
		//Main Menu Stage Icons (04042015)
		assetManager.load("Icons/playIcon.png", Texture.class, param);
		assetManager.load("Icons/leaderboardIcon.png", Texture.class, param);
		assetManager.load("Icons/creditIcon.png", Texture.class, param);
		assetManager.load("Icons/researchIcon.png", Texture.class, param);
		assetManager.load("Icons/rateIcon.png", Texture.class, param);
		
		//credit iamge
		assetManager.load("Icons/creditImage.png", Texture.class, param);
		
	}

	private static void loadButtons(){
		assetManager.load("ButtonImage/nspeedIcon.png", Texture.class, param);
		assetManager.load("ButtonImage/dspeedIcon.png", Texture.class, param);
		assetManager.load("ButtonImage/pauseIcon.png", Texture.class, param);
		assetManager.load("ButtonImage/playIcon.png", Texture.class, param);
		assetManager.load("ButtonImage/menuIcon.png", Texture.class, param);
		assetManager.load("ButtonImage/ultIcon.png", Texture.class, param);
	}
	
	private static void playScreenImages(){
		assetManager.load("playBg.jpg", Texture.class);
	}

	
	public static void dispose(){
		//assetManager.dispose();
		assetManager = null;
	}
	
	
}
