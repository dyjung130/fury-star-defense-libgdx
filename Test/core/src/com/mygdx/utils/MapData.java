package com.mygdx.utils;

import java.util.Random;

public class MapData {
	
	/*Map data int[][] must be final!
	 * static references the same memory location
	 * so it will corrupt the data when called again or reset.
	 */
	private final int[][] SPRITES_INDEX1 = { 
			{ 1,-1, 4, 0, 0, 5,-1, 8, 0, 9},
			{ 0,-1, 0,-1,-1, 6, 0, 7,-1, 0},
			{ 2, 0, 3,-1,13, 0, 0,12,-1, 0},
			{-1,-1,-1,-1, 0,-1,-1,11, 0,10},
			{17, 0,16,-1, 0,-1,-1,-1,-1,-1},
			{ 0,-1,15, 0,14,-1,-1,22, 0,23},
			{ 0,-1,-1,-1,-1,-1,-1, 0,-1, 0},
			{ 0,-1,20, 0, 0, 0, 0,21,-1, 0},
			{18, 0,19,-1,-1,-1,-1,-1,-1,24} 
		};
	
	private final int[][] SPRITES_INDEX2 = {
			{ 1, 0, 2,-1,-1,-1, 9, 0,10,-1},
			{-1,-1, 0,-1,-1, 7, 8,-1, 0,-1},
			{-1, 4, 3,-1,-1, 0,-1,-1,11,12},
			{-1, 0,-1,-1,-1, 0,-1,-1,-1, 0},
			{-1, 0,-1,-1,-1, 0,-1,-1,-1, 0},
			{-1, 5, 0, 0, 0, 6,-1,-1,-1, 0},
			{18, 0, 0,17,-1,-1,14, 0, 0,13},
			{ 0,-1,-1,16, 0, 0,15,-1,-1,-1},
			{19, 0, 0, 0, 0, 0, 0, 0, 0,20}
	};
	
	private final int[][] SPRITES_INDEX3 = {
		{ 1, 0, 2,-1, 5, 0, 0, 6,-1,-1},
		{-1,-1, 0,-1, 0,-1,-1, 0,-1,-1},
		{-1,-1, 3, 0, 4,-1,-1, 7, 8,-1},
		{-1,12, 0, 0, 0,11,-1,-1, 0,-1},
		{-1, 0,-1,-1,-1,10, 0, 0, 9,-1},
		{-1, 0,-1,-1,-1,-1,17, 0,18,-1},
		{-1, 0,-1,15, 0, 0,16,-1, 0,-1},
		{-1,13, 0,14,-1,-1,-1,-1, 0,-1},
		{20, 0, 0, 0, 0, 0, 0, 0,19,-1}
	};
	
	public int[][] getData(){
		int[][] temp = SPRITES_INDEX1;
		Random r = new Random();

	        int i = r.nextInt()%3;

	        switch (i) {
	            case 0:
	            	temp = SPRITES_INDEX1;
	            	break;
	            case 1:
	            	temp = SPRITES_INDEX2;
	            	break;
	            case 2:
	            	temp = SPRITES_INDEX3;
	            	break;
	            default:
	            	temp = SPRITES_INDEX3;
	            	break;
	        }
	        
	     return temp;
	}
		
}
