package com.mygdx.utils;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;

public enum TowerType {

	SINGLE(Constants.SINGLE_1, 120, 30, 250, 0.8f, 600, Assets.getAssetManager().get("TowerImage/single1.png", Texture.class), 2f, EffectType.ARMORv1),
	SINGLE1E(Constants.SINGLE_2A, 120, 150, 150, 0.8f, 600, Assets.getAssetManager().get("TowerImage/single2a.png", Texture.class), 2f, EffectType.ARMORv2),
	SINGLE2E(Constants.SINGLE_2B, 200, 35, 150, 0.4f, 600, Assets.getAssetManager().get("TowerImage/single2b.png", Texture.class), 2f, EffectType.ARMORv1),

	MULTI(Constants.MULTI_1, 140, 10, 500, 0.8f, 600, Assets.getAssetManager().get("TowerImage/multi1.png", Texture.class), 10f, EffectType.MULTI, 5),
	MULTI1E(Constants.MULTI_2A, 150, 20, 200, 0.8f, 600, Assets.getAssetManager().get("TowerImage/multi2a.png", Texture.class), 10f, EffectType.DEATH, 5),
	MULTI2E(Constants.MULTI_2B, 140, 20, 200, 0.8f, 600, Assets.getAssetManager().get("TowerImage/multi2b.png", Texture.class), 10f, EffectType.BURN, 6),
	//float fireRate, float duration, float coolDown, 
	AREA(Constants.AREA_1, 135, 0, 1000, 3f, 2, Assets.getAssetManager().get("TowerImage/area1.png", Texture.class), 10f, EffectType.SLOW),
	AREA1E(Constants.AREA_2A, 135, 0, 250, 5f, 2, Assets.getAssetManager().get("TowerImage/area2a.png", Texture.class), 10f, EffectType.FREEZE),
	AREA2E(Constants.AREA_2B, 135, 0, 250, 6f, 2, Assets.getAssetManager().get("TowerImage/area2b.png", Texture.class), 10f, EffectType.TELE),
	
	UTIL(Constants.UTIL_1, 500, 1500, 12f, Assets.getAssetManager().get("TowerImage/util1.png", Texture.class), EffectType.GOLDv1),
	UTIL1E(Constants.UTIL_2A, 2000, 2000, 12f, Assets.getAssetManager().get("TowerImage/util2a.png", Texture.class), EffectType.GOLDv2),
	UTIL2E(Constants.UTIL_2B, 2000, 2000, 12f, Assets.getAssetManager().get("TowerImage/util2b.png", Texture.class), EffectType.GOLDSCORE);
	
	private byte id; // tower id
	private int range, damage, cost, bulletSpeed, numOfTarget, collectibleGold;
	private float fireRate, coolDown;
	private Texture img;
	private EffectType effectType;
	
	
	//Utility Tower
	private TowerType(byte id, int collectibleGold, int cost, float coolDown, Texture img, EffectType effectType){
		this.id = id;
		this.collectibleGold = collectibleGold;
		this.cost = cost;
		this.coolDown = coolDown;
		this.img = img;
		this.effectType = effectType;
	}
	
	//Single Tower
	private TowerType(byte id, float range, int damage, int cost, float fireRate, int bulletSpeed, Texture img, float coolDown, EffectType effectType){
		this.id = id;
		this.range = (int) range;
		this.damage = (int) damage;
		this.cost = cost;
		this.fireRate = fireRate;
		this.bulletSpeed = bulletSpeed;
		this.img = img;
		this.coolDown = coolDown;
		this.effectType = effectType;
		
	}

	//Multi Tower
	private TowerType(byte id, float range, int damage, int cost, float fireRate, int bulletSpeed, Texture img, float coolDown, EffectType effectType, int numOfTarget){
		this.id = id;
		this.range = (int) range;
		this.damage = (int) damage;
		this.cost = cost;
		this.fireRate = fireRate;
		this.bulletSpeed = bulletSpeed;
		this.img = img;
		//img.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	//	img.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		this.coolDown = coolDown;
		this.effectType = effectType;
		this.numOfTarget = numOfTarget;
	}
	
	//for factory switch
	public byte getId(){
		return id;
	}
		
	public int getRange(){
		return  range;
	}
	
	public int getDamage(){
		return  damage;
	}
	
	public int getCost(){
		return  cost;
	}
	
	public float getFireRate(){
		return  fireRate;
	}
	
	public int getBulletSpeed(){
		return  bulletSpeed;
	}
	
	public Texture getTexture(){
		img.setFilter(TextureFilter.Linear,TextureFilter.Linear);
		return  img;
	}
	
	public float getCD(){
		return  coolDown;
	}
	
	public int getCollectible(){
		return  collectibleGold;
	}
	
	public EffectType getEffectType(){
		return  effectType;
	}
	
	public int getNumOfTargets(){
		return  numOfTarget;
	}
	
}
