package com.mygdx.utils;

public enum EffectType {
	
	SLOW(Constants.slow, 0.5f, 5f),
	FREEZE(Constants.freeze, 0f, 3f),
	TELE(Constants.teleport, 1f, 3f),
	ARMORv1(Constants.armorReducer, 0.95f, 3f),
	ARMORv2(Constants.armorReducerv2, 0.7f, 2f),
	BURN(Constants.burn, 1f, 2f),
	DMGAMP(Constants.damageAmplifier,2f, 0f),
	MULTI(Constants.multi, 1f, 1f),
	DEATH(Constants.instantD, 1f, 1f),
	GOLDv1(Constants.gold, 2f, 0f),
	GOLDv2(Constants.goldv2, 2f, 0f),
	GOLDSCORE(Constants.score, 2f, 0f);
	
	private float multiplier = 1f, duration = 0f;
	private short effectConst = 0;
	
	private EffectType(short effectConst, float multiplier, float duration){
		this.multiplier = multiplier;
		this.effectConst = effectConst;
		this.duration = duration;
	}
	
	
	public short getId(){
		return effectConst;
	}
	
	public float getMultiplier(){
		return multiplier;
	}
	
	public float getDuration(){
		return duration;
	}
	
	public void setDuration(float d){
		duration *= d;
	}
	
	//set multiplier when upgraded in game
	public void setMultiplier(float m){
		multiplier *= m;
	}
}
