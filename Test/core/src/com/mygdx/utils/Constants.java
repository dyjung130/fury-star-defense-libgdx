package com.mygdx.utils;

public class Constants {
	
	//main menu title
	public static final int titleWidth = 760;
	public static final int titleHeight = 190;
	//Preferences (local user data) 
	public static final String LOCAL_USERDATA = "LocalFileHEHEHE";// if not signedin
	public static final String BGM_ENABLED = "bgmEnabled";
	public static final String IS_SIGNEDIN = "isLoggedIn";
	public static final String AUTOLOGIN_ENABLED = "autoLoginEnabled";
	public static final String IS_EFFECT_ENABLED = "effectEnabled";

	//Player researches
	public static final String rH = "healthLvl";
	public static final String rD = "damageLvl";
	public static final String rF = "fireRateLvl";
	public static final String rG = "goldLvl";
	public static final String rS = "scoreLvl";
	public static final String rC = "coolDownLvl";
	
	//Player properties
	public static final String pL = "level";
	public static final String pE = "exp";
	public static final String pO = "orbs";
	public static final String pB = "bestScore";
	public static final String pI = "inGameScore";
	
	//Player option 
	public static final String pOL = "isLoggedIn";
	public static final String pOA = "isAutoLoginEnabled";
	public static final String pOB = "isBGMEnabled";
	public static final String pOE = "isEffectEnabled";
	
	//Effect Types
	//Area Effect
	public static final short slow = 1001;
	public static final short freeze = 1002;
	public static final short teleport = 1003;
	//Single Effect
	public static final short armorReducer = 1004;
	public static final short armorReducerv2 = 10055;
	//Multi Effect
	public static final short burn = 1005;
	public static final short instantD = 1006;
	public static final short multi = 10066;
	//Util Effect
	public static final short gold = 10088;
	public static final short goldv2 = 10038;
	public static final short score = 10018;
	public static final short damageAmplifier = 1007;
	

	//App Screen to Stage full size 
	public static final int APP_WIDTH = 720;
	public static final int APP_HEIGHT = 1280;
	//Background Offest
	public static final int MAP_BOTTOM = (int) (APP_HEIGHT * 11.2f/45.11f);
	//Health & Gold Asset Bar
	public static final int PLAYER_BARY = (int) (APP_HEIGHT * (29+0.5+7+0.5+3.25)/45.11f);
	public static final int PLAYER_BARGAP = (int) (APP_HEIGHT * 7/45.11f);
	//Score Bar
	public static final int SCORE_BARY = (int) (APP_HEIGHT * (1.40+25.30+50+7+0.5+3.25)/45.11f);

	public static final int UI_POS_X = 0;
	public static final int UI_POS_Y = 900;
	//Arrow Dimension
	public static final int ARROW_DIM = 60;
	//Tile Dimension
	public static final int TILE_X = (int) (APP_WIDTH * 2.8f/25.36f);
	public static final int TILE_Y = (int) (APP_WIDTH * 2.8f/25.36f);
	public static final int TOWER_DIM = (int) (APP_WIDTH * 3.6f/25.36f);
	//UI Tower Dimesion
	public static final int UI_DIM_X = (int) (APP_WIDTH * 3.0f/25.36f);
	public static final int UI_DIM_Y = (int) (APP_WIDTH * 3.0f/25.36f);	
	//UI Tower Portion Dimesion
	public static final int UI_TOWERGAP = (int) (APP_HEIGHT * 0.25f/45.11f);
	public static final int UI_TOWERLISTX = (int) (APP_WIDTH * 19.48f/25.36f);
	public static final int UI_TOWERLISTY = (int) (APP_HEIGHT * 7f/45.11f);
	public static final int UI_TOWERLISTY2 = (int) (APP_HEIGHT * 8.15f/45.11f);
	//PlayScreen ScrollBar Height
	public static final int P_SCROLLBAR = (int) (APP_HEIGHT * 3.25f/45.11f);
	//Screen Id
	public static final int loginScreen = -2;
	public static final int loadingScreen = -1;
	public static final int mainScreen = 0;
	public static final int playScreen = 1;
	public static final int researchScreen = 2;
	public static final int lbScreen = 3;
	public static final int optionScreen = 4;
	public static final int creditScreen = 5;
	public static final int exitScreen = 6;
	
	//Rate
	public final static float SPAWN_RATE = 0.8f;
	public final static float WAVE_RATE = 25f;

	//Player's Choice in PlayScreen.java
	public final static int GOTOMENU = 8202;
	public final static int REPLAY = 10203;
	public final static int SUBMITSCORE = 22331;
	
	//true: upgrade / false:activate ults
	public static boolean towerOption = true;
	public static float upgradeRate = 1.13f;
	public static float upgradeAreaRate = 1.4f;
	public static float collectibleRate = 1.07f;
	public static float tCollectibleRate = 1.13f;
	public static int evolveCost = 20000;
	//tower unique id
	/*level 1*/
	public final static byte SINGLE_1 = 0x01;
	public final static byte MULTI_1 = 0x02;
	public final static byte AREA_1 = 0x03;
	public final static byte UTIL_1 = 0x04;
	/*level 2a*/
	public final static byte SINGLE_2A = 0x21;
	public final static byte MULTI_2A = 0x22;
	public final static byte AREA_2A = 0x23;
	public final static byte UTIL_2A = 0x24;
	/*level 2b*/
	public final static byte SINGLE_2B = 0x41;
	public final static byte MULTI_2B = 0x42;
	public final static byte AREA_2B = 0x43;
	public final static byte UTIL_2B = 0x44;
	
	
	
}
