package com.mygdx.utils;

import com.badlogic.gdx.graphics.Texture;

public enum CreepType {

	C1(80, 200, 100, Assets.getAssetManager().get("monster/robot1.png", Texture.class)),
	C2(120, 200, 125, Assets.getAssetManager().get("monster/robot2.png", Texture.class)),
	C3(147, 200, 156, Assets.getAssetManager().get("monster/robot3.png", Texture.class)),
	C4(181, 200, 195, Assets.getAssetManager().get("monster/robot4.png", Texture.class)),
	C5(223, 200, 244, Assets.getAssetManager().get("monster/robot5.png", Texture.class)),
	C6(274, 220, 305, Assets.getAssetManager().get("monster/shadow1.png", Texture.class)),
	C7(337, 220, 381, Assets.getAssetManager().get("monster/shadow2.png", Texture.class)),
	C8(415, 220, 477, Assets.getAssetManager().get("monster/shadow3.png", Texture.class)),
	C9(611, 220, 596, Assets.getAssetManager().get("monster/shadow4.png", Texture.class)),
	C10(728, 220, 745, Assets.getAssetManager().get("monster/shadow5.png", Texture.class)),
	C11(873, 320, 931, Assets.getAssetManager().get("monster/shadow6.png", Texture.class)),
	C12(1051, 300, 1164, Assets.getAssetManager().get("monster/missile1.png", Texture.class)),
	C13(1269, 300, 1455, Assets.getAssetManager().get("monster/missile2.png", Texture.class)),
	C14(1638, 300, 1819, Assets.getAssetManager().get("monster/missile3.png", Texture.class)),
	C15(2200, 300, 2274, Assets.getAssetManager().get("monster/missile4.png", Texture.class)),
	C16(2777, 250, 2842, Assets.getAssetManager().get("monster/missile5.png", Texture.class)),
	C17(3677, 180, 3553, Assets.getAssetManager().get("monster/mecha1.png", Texture.class)),
	C18(5293, 180, 4440, Assets.getAssetManager().get("monster/mecha2.png", Texture.class)),
	C19(6000, 180, 5550, Assets.getAssetManager().get("monster/mecha3.png", Texture.class)),
	C20(7000, 180, 6940, Assets.getAssetManager().get("monster/mecha4.png", Texture.class));
	
	private int health, movementSpeed, gold;
	private Texture texture;
	
	private CreepType(int health, int movementSpeed, int gold, Texture texture){
		this.health = health;
		this.movementSpeed = movementSpeed;
		this.gold = gold;
		this.texture = texture;
	}
	
	public int getHealth(){
		return health;
	}
	
	public int getSpeed(){
		return movementSpeed;
	}
	
	public int getGold(){
		return gold;
	}
	
	public Texture getTexture(){
		return texture;
	}
}
