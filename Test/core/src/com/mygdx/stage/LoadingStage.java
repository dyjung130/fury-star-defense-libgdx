package com.mygdx.stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.denden130.furystardefense.Main;
import com.mygdx.actors.BitmapFontActor;
import com.mygdx.utils.Constants;

public class LoadingStage extends Stage{

	private Image loadingIcon, logo;
	private int degrees = 4, iconWidth = 256 , iconHeight = 256;
	private Texture loadingIconTexture, logoTexture;;
	private BitmapFontActor loadingProgress;
	private boolean startTiming, isLoadingAnimationDone, isFlashScreenEnded;
	
	public LoadingStage(final Main main){
		super(new ScalingViewport(Scaling.stretch, Constants.APP_WIDTH, Constants.APP_HEIGHT));
		
		//loading assets
		loadingIconTexture = new Texture(Gdx.files.internal("UtilityImage/loadingIcon.png"));
		loadingIcon = new Image(loadingIconTexture);
		loadingIcon.setBounds(Constants.APP_WIDTH/2 -iconWidth/2f, Constants.APP_HEIGHT/2 - iconWidth/2f, iconWidth, iconHeight);
		loadingIcon.setOrigin(iconWidth/2f, iconHeight/2f);
		loadingProgress = new BitmapFontActor(Constants.APP_WIDTH/2 - 15, Constants.APP_HEIGHT/2 - 30, 30, 30, 0, "");
		loadingProgress.setScale(2f);
		setupLoading();
		
		logoTexture = new Texture(Gdx.files.internal("logo.png"));
		logo = new Image(logoTexture);
		logo.setBounds(Constants.APP_WIDTH/2 -iconWidth/2f, Constants.APP_HEIGHT/2 - iconWidth/2f, iconWidth, iconHeight);
		logo.setOrigin(iconWidth/2f, iconHeight/2f);
		logo.addAction(Actions.parallel(Actions.fadeOut(2f)));
	}
	
	public void addLoadingActions(){
		loadingIcon.addAction(Actions.parallel(Actions.fadeOut(2f)));
		startTiming = true;
		loadingProgress.clear();
		loadingProgress.remove();
	}
	
	private void setupLoading(){
		addActor(loadingIcon);
		addActor(loadingProgress);
	}
	
	private void setupFlashScreen(){
		addActor(logo);
	}
	
	public void switchToFlashScreen(){
		if(isLoadingAnimationDone == true){
			disposeLoadingAssets();
			setupFlashScreen();
		}
	}
	
	public boolean flashScreenEnded(){
		return isFlashScreenEnded;
	}
	
	@Override
	public void draw(){
		super.draw();
	}
	
	public void updateTime(byte progress){
		if(isFlashScreenEnded == false){
			loadingProgress.updateValue(progress);
		}
	}
	float loadingAnimaTimer = 0f;
	@Override
	public void act(float delta){
		super.act(delta);
		if(isFlashScreenEnded == false){
			loadingIcon.rotateBy(degrees);
		}
		if(logo.getActions().size == 0){
			isFlashScreenEnded = true;
		}
		if(startTiming == true){
			loadingAnimaTimer += delta;
		}
		if(loadingAnimaTimer > 3f){
			isLoadingAnimationDone = true;
		}
	}
	
	public void disposeLoadingAssets(){
		loadingIcon.clear();
		loadingIcon.remove();
		loadingIconTexture.dispose();
		
	}
	
	@Override
	public void dispose(){
		super.dispose();
		logo.clear();
		logo.remove();
		logoTexture.dispose();
		
	}
}
