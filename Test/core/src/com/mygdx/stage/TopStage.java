package com.mygdx.stage;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.denden130.furystardefense.Main;
import com.mygdx.actors.BitmapFontActor;
import com.mygdx.actors.BackGround;
import com.mygdx.factories.TowerFactory;
import com.mygdx.handler.ScreenCallbackHandler;
import com.mygdx.player.MyPlayer;
import com.mygdx.userinterface.ResearchUI;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;
import com.mygdx.utils.TowerType;


public class TopStage extends Stage{
	
	private static TopStage topStageInstance;
	public static boolean botStageDragged, topStageDragged;
	private BitmapFontActor userHealth, userMoney, userScore, userWave, nextWaveTimer;
	private Sprite selectedTile, prevSelectedTile;
	private int[][] mapProp; // contains current map property (constructable? upgradable?)
	private int tempMapProp; // contains the prop whether it is constructable tile
	private int mapLocX, mapLocY; // position where mapProp value needs to be changed
	
	private int tileX = Constants.TOWER_DIM;
	private int tileY = Constants.TOWER_DIM;
	private Vector2 actorDim = new Vector2(Constants.TOWER_DIM, Constants.TOWER_DIM);
	private int posDiff = (Constants.TOWER_DIM - Constants.TILE_X)/2;
	private List<TowerType> towerTypeList;
	
	private MyPlayer player;
	private Main main;
	//Top UI
	private Image topUI;
	//Buttons available on UI stage
	private Image researchButton, stateControlButton, speedControlButton;
	//Buttons available inside the research UI
	private ScreenCallbackHandler scbh;
	private int playerMoney = 2000;//initial money needs to fix later with player's property
	private int playerHealth = 10; //initial health needs to fixed ?
	private int playerScore = 0; //inital score is always 0
	
	private boolean speedUp = false, isPaused = false, backPressed = false;

	//tower pointer is added to this class
	private Image towerPointer = new Image(Assets.getAssetManager().get("UtilityImage/towerPointer.png", Texture.class));
	
	private Batch batch = new SpriteBatch();
	private boolean drawParticleEffect = false;
	private ParticleEffect pe = new ParticleEffect();
	
	
	
	private TopStage(final Main main, MyPlayer player){
		super(new ScalingViewport(Scaling.stretch, Constants.APP_WIDTH, Constants.APP_HEIGHT));
		scbh = main;
		mapProp = BackGround.getInstance().getSpritesProp();
		
		this.main = main;
		this.player = player;
	
		setActorTypeList();
		initializeUI();
		ResearchUI.getInstance(this);
		setTowerImgList();
		
		//set properties of the towerPointer
		towerPointer.setSize(50, 100);
		towerPointer.setPosition(-300, -400);//hide it in the beginning
		//animation to point to the actor up & down
		towerPointer.addAction(Actions.forever(Actions.sequence(Actions.moveBy(0, 20, 0.2f), Actions.moveBy(0, -20, 0.3f))));
		addActor(towerPointer);
		//initialize research ui
		
		
	}

	private void setActorTypeList(){
		towerTypeList = new ArrayList<TowerType>();
		towerTypeList.add(TowerType.AREA);
		towerTypeList.add(TowerType.UTIL);
		towerTypeList.add(TowerType.SINGLE);
		towerTypeList.add(TowerType.MULTI);
	}
	
	public static synchronized TopStage getTopStageInstance(final Main main, MyPlayer player){
		if(topStageInstance == null){
			topStageInstance= new TopStage(main, player);
		}
		return topStageInstance;
	}
	
	public static TopStage getInstance(){
		return topStageInstance;
	}
	
	public int getMoney(){
		return playerMoney;
	}
	
	public void updateMoney(int cost){
		playerMoney -= cost;
	}
	
	public int getHealth(){
		return playerHealth;
	}
	
	public void updateHealth(int target){
		playerHealth -= target;
	}
	
	public int getScore(){
		return playerScore;
	}
	
	public void updateScore(int score){
		playerScore += score;
	}
	
	public boolean isBackPressed(){
		return backPressed;
	}
	
	private void initializeUI(){
		
		topUI= new Image(Assets.getAssetManager().get("BackgroundImage/topUI.png", Texture.class));
		topUI.setBounds(0, Constants.APP_HEIGHT*6/7, Constants.APP_WIDTH, 200);
		
		addActor(topUI);
		
		
		//above the tower selection UI
		userMoney = new BitmapFontActor(Constants.APP_WIDTH/2 - 160, 240, 
				Constants.APP_WIDTH, 60, playerMoney, Assets.getAssetManager().get("Icons/gold.png", Texture.class));
		
		//first row UI
		userScore = new BitmapFontActor(190, 
				Constants.PLAYER_BARY, 120, 60, playerScore, Assets.getAssetManager().get("Icons/score.png", Texture.class));//starts with 0 for score
		
		//second row UI
		userHealth = new BitmapFontActor(155, Constants.PLAYER_BARY + Constants.PLAYER_BARGAP/4 + 20, 
				Constants.APP_WIDTH, 60, playerHealth, Assets.getAssetManager().get("Icons/health.png", Texture.class));
		userWave = new BitmapFontActor(Constants.APP_WIDTH/2 - 65, 
				Constants.PLAYER_BARY + Constants.PLAYER_BARGAP/4 + 20, 120, 60, 0, Assets.getAssetManager().get("Icons/wave.png", Texture.class));//starts with 0 for wave
		nextWaveTimer = new BitmapFontActor(Constants.APP_WIDTH/2 + 75, 
				Constants.PLAYER_BARY + Constants.PLAYER_BARGAP/4 + 20, 120,60, 0, Assets.getAssetManager().get("Icons/time.png", Texture.class));
		

		//add actors to the stage
		addActor(userHealth);
		addActor(userScore);
		addActor(userMoney);
		addActor(userWave);
		addActor(nextWaveTimer);
		
		stateControlButton = new Image(Assets.getAssetManager().get("ButtonImage/pauseIcon.png", Texture.class));
		researchButton = new Image(Assets.getAssetManager().get("ButtonImage/ultIcon.png", Texture.class));
		speedControlButton = new Image(Assets.getAssetManager().get("ButtonImage/nspeedIcon.png", Texture.class));

		stateControlButton.setBounds(Constants.APP_WIDTH - 100, Constants.APP_HEIGHT - 330,  110, 70);
		researchButton.setBounds(Constants.APP_WIDTH - 100, Constants.APP_HEIGHT - 410,  110, 70);
		speedControlButton.setBounds(Constants.APP_WIDTH - 100, Constants.APP_HEIGHT - 250, 110, 70);
		
		stateControlButton.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				
				return true; 
			}
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				super.touchUp(event, x, y, pointer, button);
				
					if(isPaused == false){
						isPaused = true;
						BottomStage.getStageInstance(main).setSpeedFactor(0f);
						ProjectileStage.getInstance().setSpeedFactor(0f);
						stateControlButton.setDrawable(new SpriteDrawable(new Sprite (Assets.getAssetManager().get("ButtonImage/playIcon.png", Texture.class))));
					}else{
						isPaused = false;
						BottomStage.getStageInstance(main).setSpeedFactor(1f);
						ProjectileStage.getInstance().setSpeedFactor(1f);
	
						speedUp = false;
						stateControlButton.setDrawable(new SpriteDrawable(new Sprite (Assets.getAssetManager().get("ButtonImage/pauseIcon.png", Texture.class))));
						speedControlButton.setDrawable(new SpriteDrawable(new Sprite (Assets.getAssetManager().get("ButtonImage/nspeedIcon.png", Texture.class))));
					}
				}
			
			
		});
		addActor(stateControlButton);
		
		researchButton.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				
				if(Constants.towerOption == false){
					Constants.towerOption = true;
					researchButton.setColor(Color.WHITE);
				}else{
					Constants.towerOption = false;
					researchButton.setColor(Color.RED);
				}
				return true; 
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				super.touchUp(event, x, y, pointer, button);
				
			}
		});
		
		addActor(researchButton);
		
		speedControlButton.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				
				return true; 
			}
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				super.touchUp(event, x, y, pointer, button);
				if(isPaused == false){
					if(speedUp == false){
						speedUp = true;
						BottomStage.getStageInstance(main).setSpeedFactor(2f);
						ProjectileStage.getInstance().setSpeedFactor(2f);
						speedControlButton.setDrawable(new SpriteDrawable(new Sprite (Assets.getAssetManager().get("ButtonImage/dspeedIcon.png", Texture.class))));
					}else{
						speedUp = false;
						BottomStage.getStageInstance(main).setSpeedFactor(1f);
						ProjectileStage.getInstance().setSpeedFactor(1f);
						speedControlButton.setDrawable(new SpriteDrawable(new Sprite (Assets.getAssetManager().get("ButtonImage/nspeedIcon.png", Texture.class))));
					}
				}
			}
		});
		addActor(speedControlButton);
		
	}
	 
	//update bitmapfont actors
	private void updateDisplayData(){
		userHealth.updateValue(playerHealth);
		userScore.updateValue(playerScore);
		userMoney.updateValue(playerMoney);
		userWave.updateValue(BottomStage.getStageInstance(main).getWaveNumber());
		nextWaveTimer.updateValue(BottomStage.getStageInstance(main).getWaveTimer());
	}
	
	
	/* set tower image list at the bottom UI
	 * on dragged the image can be moved to desired position
	 */
	private void setTowerImgList(){
		int posX =200;
		int posY = 15;
		int width = Constants.TOWER_DIM - 5;
		int height = width;
		
		for(TowerType a : towerTypeList){
		
			Texture t  = a.getTexture();
			final TowerType tempType = a;
			final Image img = new Image(t);
			final Texture tempT = t;
			final int localPosX = posX;
			final int localPosY = posY;
			final int localWidth = width;
			final int localHeight = height;
			
			img.setBounds(localPosX, localPosY, localWidth, localHeight );
			
			final BitmapFontActor costBitmapFont = new BitmapFontActor(localPosX, localPosY + 20, localWidth, localHeight, a.getCost(), "Cost-");
			
			Image unitTypeIcon;
			if(a == TowerType.AREA){
				unitTypeIcon = new Image(Assets.getAssetManager().get("Icons/unitArea.png", Texture.class));
			}else if(a == TowerType.MULTI){
				unitTypeIcon = new Image(Assets.getAssetManager().get("Icons/unitMulti.png", Texture.class));
			}else if(a == TowerType.SINGLE){
				unitTypeIcon = new Image(Assets.getAssetManager().get("Icons/unitSingle.png", Texture.class));
			}else{
				unitTypeIcon = new Image(Assets.getAssetManager().get("Icons/unitUtil.png", Texture.class));
			}
			unitTypeIcon.setBounds(localPosX + 90, localPosY + 20, localWidth*3/4, localHeight*3/4);
			unitTypeIcon.setColor(Color.CYAN);
			addActor(unitTypeIcon);
		
			addActor(img);
			addActor(costBitmapFont);
			img.addListener(new ClickListener(){
				Image imgCopy;
				
				@Override
				public boolean touchDown(InputEvent e, float x, float y, int pointer, int button){
					super.touchDown(e, localPosX, localPosY, pointer, button);
					botStageDragged = true;
					return true;
				}
				
				@Override
				public void touchDragged(InputEvent e, float x, float y, int pointer){
					super.touchDragged(e, x, y, pointer);
					botStageDragged = true;
					
					//coordinates synced for all stages. compensates for new camera pos
					Vector2 coords = new Vector2(localPosX + x + ProjectileStage.getCamPosChange().x, localPosY + y + ProjectileStage.getCamPosChange().y);
					
					if(imgCopy == null){
						imgCopy  = new Image(tempT);
					}
					
					imgCopy.setSize(tileX, tileY);
					imgCopy.setPosition(localPosX + x - 20, localPosY + y + 70);
					TopStage.getTopStageInstance(main, player).addActor(imgCopy);
					
					//On dragging & hovering over a tile, it is highlighted
					Sprite[][] map = BackGround.getInstance().getSpriteMap();
					
					outerloop:
					for(int xx = 0; xx < map.length; xx++){
						for(int yy = 0; yy < map[0].length; yy++){
							if(map[xx][yy].getBoundingRectangle().contains(coords)){
								
								selectedTile = map[xx][yy];
								
								if(selectedTile != prevSelectedTile && prevSelectedTile != null){
									prevSelectedTile.setColor(Color.WHITE);
								}
								//map property -2 : tower is constructed already/ upgrade available
								if(mapProp[xx][yy] == -2){
									tempMapProp = -2; // upgradable tile
								}
								
								//map property -1: tower can be constructed at this position
								if(mapProp[xx][yy] == -1){
								
									coords.x = selectedTile.getX();
									coords.y = selectedTile.getY();
									selectedTile.setColor(Color.RED);
									tempMapProp = mapProp[xx][yy];
									mapLocX = xx;
									mapLocY = yy;
						
									prevSelectedTile = selectedTile;
								}
								
								if(mapProp[xx][yy] != -1){
									tempMapProp = 0;
								}
								
								break outerloop;
							}
						}
					}
					//--On dragging & hovering over a tile, it is highlighted--end
				}
				
				@Override
				public void touchUp(InputEvent e, float x, float y, int pointer, int button){
					super.touchUp(e, x, y, pointer, button);
				
					
					Vector2 coordinates = new Vector2();
				
					//Tower Construction and Highlighting the Selected Tile
					if(selectedTile != null){
						coordinates = new Vector2(selectedTile.getX() - posDiff, selectedTile.getY() - posDiff);
						
					}		
						//add tower based on the ActorType 
						//needs condition . If money > 0 & cost > money
						if(tempMapProp == -1 && (playerMoney-tempType.getCost() >= 0)){
							BottomStage.getStageInstance(main).addTower(TowerFactory.getInstance().getTower(coordinates, actorDim, tempType));
							updateMoney(tempType.getCost());
							mapProp[mapLocX][mapLocY] = -2;// once constructed change map property. check if it is welldefined..
							tempMapProp = -2;
						}
						
						if(selectedTile != null && tempMapProp < 0 ){
							selectedTile.setColor(Color.WHITE); //on making the tower, remove the trace of the highlighted panel
						}
						
						if(imgCopy != null){
							imgCopy.remove();
							imgCopy.clear();
							imgCopy = null;
						}
						botStageDragged = false;
				}
			});
	
			if(posX + width < Constants.APP_WIDTH/2){
				posX = Constants.APP_WIDTH/2;
			}else{
				posX = 200;
				posY += localHeight;
			}
		}
	}
	
	
	public void movePointer(float f, float g){
		towerPointer.setPosition(f, g);
	}
	
	
	public void setParticleEffect(ParticleEffect pe){
		this.pe = pe;
		drawParticleEffect = true;
	}
	
	@Override
	public void draw(){
		super.draw();
		batch.begin();
		if(drawParticleEffect == true){
			pe.draw(batch);
			pe.update(Gdx.graphics.getDeltaTime());
		}
		batch.end();
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		updateDisplayData();
	}

	@Override
	public void dispose(){
		super.dispose();
		//remove UIs
		ResearchUI.getInstance().clearAll();
		topUI.remove();
		userHealth.remove();
		userMoney.remove();
		userScore.remove();
		userWave.remove();
		nextWaveTimer.remove();
		topStageInstance = null;
		towerPointer.remove();

	}

}
