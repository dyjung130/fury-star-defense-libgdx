package com.mygdx.stage;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.denden130.furystardefense.Main;
import com.mygdx.handler.ScreenCallbackHandler;
import com.mygdx.player.MyPlayer;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;

public class MainMenuStage extends Stage{
	
	
	private static final int ICON_DIM = 145;

	private ScreenCallbackHandler scbh;
	private Main mainGooglePlay;
	
	private static MainMenuStage instance;
	private Image mainTitle, playButton, rateButton,  leaderBoardButton, creditButton, bgImage, creditImage;//creditButton  exitButton, 
	private boolean isTouched, creditTouched;
	private MyPlayer player;
	public float creditImageDim = Constants.APP_WIDTH/5 *2;
	
	private MainMenuStage(final Main main, MyPlayer player){
		//Singleton Pattern
		super(new ScalingViewport(Scaling.stretch, Constants.APP_WIDTH, Constants.APP_HEIGHT));
		scbh = main;
		mainGooglePlay = main;
		bgImage = new Image(Assets.getAssetManager().get("BackgroundImage/fullbgtest.png", Texture.class));
		bgImage.setBounds(0, 0, Constants.APP_WIDTH, Constants.APP_HEIGHT);
		creditImage = new Image(Assets.getAssetManager().get("Icons/creditImage.png", Texture.class));
		creditImage.setBounds(0,0,0,0);
		creditImage.addAction(Actions.forever(Actions.sequence(Actions.fadeIn(1f, Interpolation.exp10), Actions.fadeOut(0.5f, Interpolation.exp10In))));
		this.player = player;

		addActors();
		setupButtons();
	}
	
	public static MainMenuStage getInstance(final Main main, MyPlayer player){
		if(instance == null){
			instance = new MainMenuStage(main, player);
		}
		return instance;
	}

	private void addActors(){
		addActor(bgImage);

	}
	
	@Override
	public void draw(){
		super.draw();

	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		if(scbh.isSignedIn()){
			leaderBoardButton.setColor(Color.WHITE);
		}else{
			leaderBoardButton.setColor(Color.BLACK);
		}
		if(creditTouched == true){
			creditImage.setBounds(Constants.APP_WIDTH/2 - creditImageDim/2, Constants.APP_HEIGHT/11, creditImageDim, creditImageDim);
			creditImage.act(delta);
		}else{
			creditImage.setPosition(-1000, -1000);
		}
	}

	@Override
	public void clear(){
		super.clear();
	}
	

	@Override
	public void dispose(){
		super.dispose();
		//remove all images
		mainTitle.remove();
		creditImage.remove();
		playButton.remove();
		rateButton.remove();
		leaderBoardButton.remove();
		creditButton.remove();
		bgImage.remove();
				
	}


	
	private void setupButtons(){

		//main title
		mainTitle = new Image(Assets.getAssetManager().get("BackgroundImage/title.png", Texture.class));
		//Initialize buttons
		playButton = new Image(Assets.getAssetManager().get("Icons/playIcon.png", Texture.class));
		rateButton = new Image(Assets.getAssetManager().get("Icons/rateIcon.png", Texture.class));
		leaderBoardButton = new Image(Assets.getAssetManager().get("Icons/leaderboardIcon.png", Texture.class));
		creditButton = new Image(Assets.getAssetManager().get("Icons/creditIcon.png", Texture.class));
		mainTitle.setSize(Constants.titleWidth, Constants.titleHeight);
		mainTitle.setPosition(Constants.APP_WIDTH/2 - Constants.titleWidth/2, Constants.APP_HEIGHT - Constants.titleHeight*2f);
		addActor(mainTitle);
		
		//Position buttons
		creditButton.setSize(ICON_DIM - 20, ICON_DIM - 20);
		creditButton.setPosition(55, 312);
		addActor(creditButton);
		
		playButton.setSize(ICON_DIM + 10, ICON_DIM + 10);
		playButton.setPosition(175, 470);
		addActor(playButton);
		
		leaderBoardButton.setSize(ICON_DIM, ICON_DIM);
		leaderBoardButton.setPosition(390, 450);
		addActor(leaderBoardButton);
		
		rateButton.setSize(ICON_DIM, ICON_DIM);
		rateButton.setPosition(535, 290);
		addActor(rateButton);
	
		//add creditImage
		addActor(creditImage);
		
		creditButton.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				//Ensures that two buttons can't be touched at the same time.
				if(creditTouched == false){
					creditTouched = true;
					creditButton.setColor(Color.CYAN);
				}
				
				return creditTouched;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				super.touchUp(event, x, y, pointer, button);
				if(creditTouched == true){
					creditTouched = false;
					creditButton.setColor(Color.WHITE);
				}
			}
	
		});
		
		playButton.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				//Ensures that two buttons can't be touched at the same time.
				if(isTouched == false){
					isTouched = true;
					playButton.setColor(Color.CYAN);
				}
				
				return isTouched;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				super.touchUp(event, x, y, pointer, button);
				
				if(isTouched == true){
					isTouched = false;
					playButton.setColor(Color.WHITE);
					scbh.selectScreen(Constants.loadingScreen, Constants.playScreen, player);
					
				}
				
			}
	
		});
		
		rateButton.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				//Ensures that two buttons can't be touched at the same time.
				if(isTouched == false){
					isTouched = true;
					rateButton.setColor(Color.CYAN);
				}
				
				return isTouched;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				super.touchUp(event, x, y, pointer, button);
				
				if(isTouched == true){
					isTouched = false;
					rateButton.setColor(Color.WHITE);
					mainGooglePlay.rateGame();
				}
		
			}
	
		});
		
		
		leaderBoardButton.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				//Ensures that two buttons can't be touched at the same time.
				if(isTouched == false){
					isTouched = true;
					leaderBoardButton.setColor(Color.CYAN);
				}
				
				return isTouched;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				super.touchUp(event, x, y, pointer, button);
				
				if(isTouched == true){
					isTouched = false;
					leaderBoardButton.setColor(Color.WHITE);
					mainGooglePlay.openLeaderBoard();
				}
			}
			
		});
		
		
		
	}
}
