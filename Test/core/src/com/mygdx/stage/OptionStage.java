package com.mygdx.stage;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.denden130.furystardefense.Main;
import com.mygdx.handler.ScrollerCallbackHandler;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;


public class OptionStage extends Stage implements ScrollerCallbackHandler{

	private static OptionStage instance;

	private Image optionButton; //background, 
	private int dropPointY = Constants.APP_HEIGHT/2;
	private Table table;
	private WidgetGroup group;
	private float totalTime;
	private boolean exitButtonTouched;
	private ScrollerCallbackHandler scbh;
	private boolean isTouched, startActing;
	private float buttonDim = 95;

	
	private OptionStage(final Main main){
		super(new ScalingViewport(Scaling.stretch, Constants.APP_WIDTH, Constants.APP_HEIGHT));
		scbh = main;
	//	background = new Image(Assets.getAssetManager().get("uiTexture.jpg", Texture.class));
		setupStage();
		addActors();
	
	}

	private void addActors(){
		optionButton = new Image(Assets.getAssetManager().get("UtilityImage/optionIcon.png", Texture.class));
		optionButton.setBounds(Constants.APP_WIDTH - 120, Constants.APP_HEIGHT - 110, buttonDim, buttonDim);
		optionButton.setOrigin(buttonDim/2, buttonDim/2);
		optionButton.setColor(Color.CYAN);
		addActor(optionButton);
	}
	
	public static synchronized OptionStage getInstance(final Main main){
		if(instance == null){
			instance = new OptionStage(main);
		}
		
		return instance;
	}
	
	private void setupStage(){
		table = new Table();
		group = new WidgetGroup();
		
		table.setBounds(Constants.APP_WIDTH/4, Constants.APP_HEIGHT*5/3, Constants.APP_WIDTH/2, Constants.APP_WIDTH/3);
		
	//	table.add(background).expand().fill();
		
		table.setDebug(true);
		Image exitButton = new Image( Assets.getAssetManager().get("UtilityImage/exitIcon.png", Texture.class));
		
		exitButton.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				 
				exitButtonTouched = true;
				
				return false;
			}
		});
		
		exitButton.setWidth(40);
		exitButton.setHeight(40);
		
		//group.addActor(exitButton);
		table.debug().right().top().add(group);
		table.row();
			
		addActor(table);

	}
	
	private void tableMoveBy(float delta){
		
		if(table.getY() > dropPointY && exitButtonTouched == false && isTouched == false){
			table.moveBy(0, -100 + totalTime * 100f);
			totalTime += delta;
			
		}
		
		if(table.getY() < 1500 && exitButtonTouched == true){
			table.moveBy(0, 90);
			totalTime = 0f;
			
		}
		
	}
	

	@Override 
	public void act(float delta){ 
		super.act(delta);
		
		if(startActing)
			tableMoveBy(delta);
				
		//for debugging purposes
		table.debug();
		table.debugTable();
		optionButton.rotateBy(-2);
			
	}
	
	@Override
	public void draw(){
		super.draw();
	
	}
	
	@Override
	public void clear(){
		super.clear();
	}
	@Override
	public void dispose(){
		super.dispose();
		table.clear();
		table.remove();
		instance = null;
	}

	@Override
	public void callMoveBy(float deltaPosX, float deltaPosY) {
		// TODO Auto-generated method stub
		scbh.callMoveBy(deltaPosX , deltaPosY);
	
	}


}
