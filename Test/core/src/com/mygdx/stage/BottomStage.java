package com.mygdx.stage;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.denden130.furystardefense.Main;
import com.mygdx.actors.BackGround;
import com.mygdx.handler.ScrollerCallbackHandler;
import com.mygdx.actors.CreepSoul;
import com.mygdx.actors.AbstractTower;
import com.mygdx.actors.AbstractCreep;
import com.mygdx.creep.CreepPathArrow;
import com.mygdx.creep.EnemyWave;
import com.mygdx.utils.Constants;

public class BottomStage extends Stage implements ScrollerCallbackHandler{
	private final float spawnRate = Constants.SPAWN_RATE, waveRate = Constants.WAVE_RATE;
	private static BottomStage testStage;
	private boolean isGameOver = false, isGameComplete = true;
	private float moveByX, moveByY, prevX, prevY, speedFactor = 1f;
	
	private BackGround backgroundActor = BackGround.getInstance();
	private OrthographicCamera camera;
	
	private float totalTime = 0f, waveTimer = 0f, spawnTimer = 0f;
		
	//List for targets, waves, and towers
	private List<AbstractCreep> targetList;
	private List<AbstractCreep> waveList = new ArrayList<AbstractCreep>();
	private List<AbstractTower> towerList = new ArrayList<AbstractTower>();
	private List<CreepPathArrow> pathArrowList = new ArrayList<CreepPathArrow>();
	private List<CreepSoul> deadCreepActions = new ArrayList<CreepSoul>();
	
	
	private float arrowTimer = 0f; private boolean started;
	
	private int waveNumber, targetSpawned, targetDestroyed, targetAtDestination;
	private static float prevScreenX, prevScreenY;
	private EnemyWave enemyWave;
	
	private int tileX = Constants.TILE_X;
	private int tileY = Constants.TILE_Y;
	private Vector2 actorDim = new Vector2(tileX, tileY);
	
	private BottomStage(final Main main){
		//ScalingViewport to expand the stage to the screen with the appropriate ratio of 720 x 480
		super(new ScalingViewport(Scaling.stretch, Constants.APP_WIDTH, Constants.APP_HEIGHT));
		enemyWave  = new EnemyWave();
		setupCamera();
		setupActor();
		Gdx.input.setInputProcessor(this);
	}

	//instance for screen switch
	public static synchronized BottomStage getStageInstance(final Main main){
		if(testStage == null){
			testStage = new BottomStage(main);
		}
		return testStage;
	}
	
	//temporary instance getter for TestActor class
	public static BottomStage getInstance(){
		if(testStage != null){
			return testStage;
		}
		return null;
	}
	
	public void setupActor(){
		//background of the scene
		addActor(backgroundActor);
	}
	
	public void setupCamera(){
		camera = new OrthographicCamera(Constants.APP_WIDTH, Constants.APP_HEIGHT);
		camera.position.set(camera.viewportWidth/2f, camera.viewportHeight/2f, 0);
		this.getViewport().setCamera(camera); // now we can control camera..
		camera.update();
	}
	
	public List<AbstractCreep> getTargetList(){
		 return targetList;
	}
	

	public void setSpeedFactor(float multiplier){
		speedFactor = multiplier;
	}
	
	@Override
	public void act(float delta){
		delta *=speedFactor;
		super.act(delta);
		if(started == false){
			updatePathArrows(delta);
		}
		updateTargets(delta);
		updateTowers(delta);
	
		if(prevX != moveByX){
			scrollBottomStage(moveByX, moveByY);
		}
		camera.update();
	}

	private void updatePathArrows(float delta){
		arrowTimer+=delta;
		
		if(arrowTimer > 0.2 && pathArrowList.size() < 20){
			pathArrowList.add(new CreepPathArrow());
			arrowTimer = 0;
		}
	
		for(ListIterator<CreepPathArrow> iterator = pathArrowList.listIterator(); iterator.hasNext();){
			CreepPathArrow temp = iterator.next();
			addActor(temp);
			
			if(temp.isReachedDestination()){
				temp.remove();
			}
			
		}
		
	}
	
	private void updateTowers(float delta){
		if(towerList.size() != 0 && !towerList.isEmpty()){//towerList.size() != 0
			for(AbstractTower tower : towerList){
				addActor(tower);
				
				//Once the tower is touched a pointer will be shown on top of the tower (04092015)
				if((Gdx.input.isTouched() || Gdx.input.isCursorCatched()) && tower.towerTouched() == true){
					TopStage.getInstance().movePointer(tower.getX() + tower.getWidth()/2 - 25, tower.getY() + tower.getHeight()/2);
					tower.resetTowerTouched(); //reset the value! (important)
				}
				
			}
		}
	}
	

	

	
	private void addDestroyedTargets(Vector2 tempV){
		TopStage.getInstance().addActor(new CreepSoul(tempV));
	//	deadCreepActions.add(soul);
	}
	
	/*private void updateDestroyedTargets(){
		if(deadCreepActions != null & deadCreepActions.size() > 0){
			for(ListIterator<CreepSoul> iterator = deadCreepActions.listIterator(); iterator.hasNext();){
				CreepSoul temp = iterator.next();
				addActor(temp);
				
				iterator.remove();
			}
		}
		
	}*/
	
	
	private void updateTargets(float delta){
		waveTimer += delta; // wave timer (resets to 0 if it is greater than wave rate)
		spawnTimer += delta; 
		totalTime += delta; // total time during the play
		
		if(targetList == null){
			targetList = new ArrayList<AbstractCreep>();
			enemyWave.setEnemyList(waveNumber);
			waveList.addAll(enemyWave.getEnemyList());
			waveNumber++;
			
		}
		
		if(waveTimer > waveRate && waveNumber < enemyWave.getNumberOfWaves()){
			enemyWave.clearEnemyList();
			enemyWave.setEnemyList(waveNumber);
			waveList.addAll(enemyWave.getEnemyList());
			waveNumber++;
			waveTimer = 0;
			started = true;
		}
		
		// Spawn rate counter
		if(spawnTimer > spawnRate && targetSpawned < waveList.size() ){
			targetList.add(waveList.get(targetSpawned));
			
			for(AbstractCreep c : targetList){
				addActor(c);	
			}
			targetSpawned++;
			spawnTimer = 0f;
	
		}
		
		
		//remove List items via ListIterator
		for(ListIterator<AbstractCreep> targetIter = targetList.listIterator(); targetIter.hasNext();){
			AbstractCreep tta = targetIter.next();	
			
			if(tta.isReachedToDest() == true || tta.isTargetDestroyed() == true){
				if(tta.isTargetDestroyed() == true){
					targetDestroyed ++;
					addDestroyedTargets(tta.getCurrentTargetPosition());
					
					TopStage.getInstance().updateMoney(-tta.getGold());
					TopStage.getInstance().updateScore((tta.getGold()));
					
				}
				if(tta.isReachedToDest() == true){
					if(TopStage.getInstance().getHealth() > 1){//if greater than 0, game continues
						targetAtDestination ++;
						TopStage.getInstance().updateHealth(1);//decrement health by 1
					}else{
						//when target reaches destination and the health is at 0 or below, the gameover message should be called
						targetAtDestination ++;
						TopStage.getInstance().updateHealth(1);
						isGameOver = true;
						
					}
					
				}
				targetIter.remove();
				tta.remove();
				tta.clear();
				}
		}
	}
	


	public void addTower(AbstractTower tower){
		//add tower to the first position of the list
		towerList.add(0, tower);
	}
	
	//this will damage the final base
	public int getNumberOfTargetAtDest(){
		return targetAtDestination;
	}
	
	//to keep up score
	public int getNumberOfTargetDestroyed(){
		return targetDestroyed;
	}
	
	//to display current wave number
	public int getWaveNumber(){
		return waveNumber;
	}
	
	public int getWaveTimer(){
		return (int)(waveRate - waveTimer);
	}
	
	//Releases all resources of this object.
	@Override
	public void dispose(){
		super.dispose();
		if(targetList != null){
			targetList.clear();
			targetList = null;
		}
		if(pathArrowList != null){
			pathArrowList.clear();
			pathArrowList = null;
		}
		if(waveList != null){
			waveList.clear(); 
			waveList = null;
		}
		
		if(towerList != null){
			towerList.clear(); 
			towerList = null;
		}
		zeroed();
		backgroundActor.remove();
		testStage = null;
		if(enemyWave != null){
			enemyWave.clearEnemyList();
		}
	}
	
	private void zeroed(){
		targetSpawned = 0; 
		targetDestroyed = 0;
		targetAtDestination = 0;
		prevScreenX = 0; 
		prevScreenY = 0;
		waveNumber = 0;
		
	}
	
	//Removes the root's children, actions, and listeners.
	@Override
	public void clear(){
		super.clear();
	}
	
	@Override
	public void draw(){
		super.draw();
		
	}
	

	public boolean getGameOverStatus(){
		return isGameOver;
	}

	public boolean getGameCompleteStatus(){
		return isGameComplete = (waveNumber >= enemyWave.getNumberOfWaves() && targetList.size() == 0)?true:false;
	}
	
	private void scrollBottomStage(float moveByX2, float moveByY2){
		
		camera.position.add(moveByX2, 0, 0f);
		ProjectileStage.getInstance().getCamera().position.add(moveByX2, 0, 0f);
		prevX = moveByX;
		prevY = moveByY;
	}

	@Override
	public void callMoveBy(float deltaPosX, float deltaPosY) {
	
		moveByX = deltaPosX;
		moveByY = deltaPosY;
	}

	
}
