package com.mygdx.stage;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.mygdx.utils.Constants;

public class ProjectileStage extends Stage{
	
	private static ProjectileStage instance;
	private static OrthographicCamera camera;
	private static Vector2 prevCamPos;
	private float speedFactor = 1f;
	
	private ProjectileStage(){
		//singleton pattern	
		super(new ScalingViewport(Scaling.stretch, Constants.APP_WIDTH, Constants.APP_HEIGHT));
		setupCamera();
		prevCamPos = new Vector2(camera.position.x , camera.position.y);
	}
		
	//set stage camera to orthographic camera
	private void setupCamera(){
		camera = new OrthographicCamera(Constants.APP_WIDTH, Constants.APP_HEIGHT);
		camera.position.set(camera.viewportWidth/2f, camera.viewportHeight/2f, 0);
		this.getViewport().setCamera(camera); // now we can control camera..
		camera.update();
		
	}
	
	//get stage instance
	public static synchronized ProjectileStage getInstance(){
		if(instance == null){
			instance = new ProjectileStage();
		}
		return instance;
	}
	
	
	//camera position to sync touched point and the touched map (bottom stage)
	public static Vector2 getCamPosChange(){
		Vector2 v = new Vector2((camera.position.x - prevCamPos.x),  (camera.position.y - prevCamPos.y));
		return v;
	}
	
	public void setSpeedFactor(float multiplier){
		speedFactor = multiplier;
	}
	
	@Override
	public void act(float delta){
		delta *= speedFactor;
		super.act(delta);
	}
	
	@Override
	public void dispose(){
		super.dispose();
		instance = null;
		
	}

}
