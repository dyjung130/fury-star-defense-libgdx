package com.mygdx.actors;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.factories.ProjectileFactory;
import com.mygdx.handler.ResearchUIHandler;
import com.mygdx.stage.BottomStage;
import com.mygdx.stage.ProjectileStage;
import com.mygdx.stage.TopStage;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;
import com.mygdx.utils.EffectType;


public abstract class AbstractTowerMulti extends AbstractTower implements ResearchUIHandler{

	public int towerLevel = 1; // tower level starts at 1
	public byte towerID = Constants.MULTI_1;
	public int xPos = 150, yPos = 200 , width = 50, height = 45, centerX, centerY; //default value
	public int range, damage, cost, baseCost, bulletSpeed, numOfTargets;
	public short towerAngle;
	public float fireRate, coolDown, ultDuration = 3f, ultTimer;
	public String projectileType;
	private boolean isHit, drawRange, ultActivated = false;
	private List<AbstractCreep> targetList;
	
	private ProjectileFactory tpf;
	private Vector2 actorPos;
	private float totalTime, cdTotalTime;
	public EffectType effectType;
	
	private List<AbstractProjectile> projectileList = new ArrayList<AbstractProjectile>();
	private List<AbstractCreep> tempLL = new ArrayList<AbstractCreep>();
	private ShapeRenderer shapeRend;
	//status indicator - cooldown bar
	private Image cdBar;
	//particle effect
	public ParticleEffect pe;
	
	
	public AbstractTowerMulti(Vector2 actorPos, Vector2 actorDim, int range, int damage, int cost, 
			float fireRate, int bulletSpeed, String projectileType, float coolDown, EffectType effectType, int numOfTargets){
		super();
	
		this.actorPos = actorPos;
		this.xPos = (int)actorPos.x;
		this.yPos = (int)actorPos.y;
		this.width = (int) actorDim.x;
		this.height = (int) actorDim.y;
		this.centerX = xPos + width/2;
		this.centerY = yPos + height/2;
	
		//actor common attributes
		this.range = range;
		this.damage = damage;
		this.cost = cost;
		baseCost = cost;
		this.fireRate = fireRate;
		this.coolDown = coolDown;
		this.bulletSpeed = bulletSpeed;
		this.projectileType = projectileType;
		this.effectType = effectType;
		this.numOfTargets = numOfTargets;
		
		this.tpf = ProjectileFactory.getInstance();
		
		this.setBounds(xPos, yPos, width, height);
		
	//	projectileList = new ArrayList<TestProjectile>();
		shapeRend = new ShapeRenderer();

		cdBar = new Image(Assets.getAssetManager().get("badlogic.jpg", Texture.class));
		cdBar.setBounds(xPos+30, yPos-5, 0, 7);
	
		//actor listener * important
		actorIsTouched();
		
		//particle effect
		pe = new ParticleEffect();
		//pe.load(Gdx.files.internal("Effect/gflame.p"),Gdx.files.internal("Effect"));
		pe.load(Gdx.files.internal("Effect/fireEffect.p"),Gdx.files.internal("Effect"));
		pe.scaleEffect(1.2f);
		pe.getEmitters().first().setPosition(centerX , centerY);
		pe.setPosition(centerX, centerY);
		pe.start();
	}
	

	@Override
	public void upgrade(){
		if(Constants.towerOption == true){
			if(TopStage.getInstance().getMoney() - cost > 0){
				towerLevel += 1;
			//	range *= 1.15f;
			//	currentLevel += 1;
				damage +=4;
				cost = (int)(baseCost * Math.pow( Constants.upgradeRate, towerLevel-1));
				TopStage.getInstance().updateMoney(cost);
				
			}else{

			}
		}
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		totalTime +=delta;
		removeTargetOutOfRange(tempLL);
		
		checkTargetInRange(targetList);
		setTargetList();
		
		drawRangeOnClick();
		
		//check if ult is available
		checkUltAvailable(delta);
		//System.out.println("te size " + tempLL.size());
		if(totalTime > fireRate){
			
			shootTarget(delta, tempLL);
			
		totalTime = 0;
		}
		
		if(cdTotalTime > 0){
			cdBar.setWidth((width-60) * cdTotalTime/coolDown);
		}
		
	}
	
	
	private boolean ultAvailable = false;
	
	
	private void checkUltAvailable(float delta){
		
		if(coolDown < cdTotalTime && ultAvailable == false){
			cdTotalTime = 0;
			ultAvailable = true;
		//	System.out.println("ult is available");
		}
		
		if(ultAvailable == false){
			cdTotalTime += delta;
		
			System.out.println("ult is NOT available");
		}
		
		//activate ult when true
		if(ultActivated == true){
			if(ultTimer <=ultDuration){
				ultTimer += delta;
			}else{
				ultTimer = 0;
				ultActivated = false;
				//reduce to original value when ended
				range /= 1.5f;
				damage /= 1.5f;
			}
		}
	}
	
	public void setDrawRange(boolean value){
		drawRange = value;
	}

	public boolean getDrawRange(){
		return drawRange;
	}
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		cdBar.draw(batch, parentAlpha);
		if(towerID != Constants.MULTI_1){
			pe.draw(batch);
			pe.update(Gdx.graphics.getDeltaTime());
		}	
			
	}

	//* draw tower's range ontouch
	private void drawRangeOnClick(){
		
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA,GL20.GL_ONE_MINUS_SRC_ALPHA);
		shapeRend.setAutoShapeType(true);
		shapeRend.begin(ShapeType.Filled);
		shapeRend.setColor(new Color(1f, 0f, 0.1f, 0.5f));
		shapeRend.setProjectionMatrix(BottomStage.getInstance().getCamera().combined);//drawn to test stage coordinates
		if(drawRange == true){
			
			shapeRend.circle(this.centerX, this.centerY, this.range);
			
		}		
		shapeRend.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);
		//* draw end
	}
	
	public void setTargetList(){

		targetList = BottomStage.getInstance().getTargetList();
	
	}

	//check if the target is in range
	public void checkTargetInRange(List<AbstractCreep> eList){
	
		if(eList != null && tempLL.size() < numOfTargets){
			
			for(AbstractCreep e: eList){
				
				if(e != null && !(tempLL.contains(e))){
					float tempX = e.getCurrentTargetPosition().x - this.centerX;
					float tempY = e.getCurrentTargetPosition().y - this.centerY;
				
					if(sqrt(tempX*tempX + tempY*tempY) < range){
						tempLL.add(e);
						break;
					}
				}
			}
		}
	}
	
	//remove actor from LinkedList..
	private void removeTargetOutOfRange(List<AbstractCreep> list){
		if(list.size() > 0){
			for(ListIterator<AbstractCreep> iterator = list.listIterator(); iterator.hasNext();){
				AbstractCreep temp = iterator.next();
				float tempX = temp.getCurrentTargetPosition().x - this.centerX;
				float tempY = temp.getCurrentTargetPosition().y - this.centerY;
				//temp != null && 
				if(sqrt(tempX*tempX + tempY*tempY) >= range || temp.isTargetDestroyed()){
					iterator.remove();
					break;
				}
			}
		}
	}
	private static double sqrt(final double a){
		final long x = Double.doubleToLongBits(a) >> 32;
		double y = Double.longBitsToDouble((x + 1072632448) << 31);

		// repeat the following line for more precision
		//y = (y + a / y) * 0.5;
		return y;
	}

	int projCounter = 0;
	private void shootTarget(float delta, List <AbstractCreep>list){
	System.out.println("Multi proj list " + projectileList.size());
		for(AbstractCreep currentTarget : list){
			if(currentTarget.getCurrentHealth() > 0){
			
				float tempX = currentTarget.getCurrentTargetPosition().x - this.centerX;
				float tempY = currentTarget.getCurrentTargetPosition().y - this.centerY;
			
				towerAngle = (short) (Math.atan2(tempY,tempX) * 57.2f + 90);
				
				if(projCounter < numOfTargets){
					projectileList.add(tpf.getTestProjectile(projectileType, currentTarget.getCurrentTargetPosition(), new Vector2(centerX, centerY), bulletSpeed));
					projCounter +=1;
				}else{
					for(ListIterator<AbstractProjectile> iterator = projectileList.listIterator();iterator.hasNext();){
						AbstractProjectile tpc = iterator.next();
						if(tpc.isVisible() == false){
							tpc.repeat(currentTarget.getCurrentTargetPosition());
						//	iterator.remove();
							break;
						}
					}
					
				}
				for(AbstractProjectile tp : projectileList){
					ProjectileStage.getInstance().addActor(tp);
					
				}
				
				
			}
			currentTarget.calculateHealth(damage);
			currentTarget.applyEffect(effectType);
		}
				

	}

	//currently to check if range needs to be drawn
	private void actorIsTouched(){
		this.addListener(new ClickListener(){
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor){
			//	if(TopStage.botStageDragged == true)
				super.enter(event, x, y, pointer, fromActor);
				drawRange = true;
				
			}
			
			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor){
				super.exit(event, x, y, pointer, toActor);
				drawRange = false;
				
				if(Constants.towerOption == false){
					
					TopStage.botStageDragged = false;
					
					//activate ult on exit
					if(ultAvailable == true){
						ultAvailable = false;
						ultActivated = true;
						range *= 1.5f;
						damage *= 1.5f;
					
					}
				}
			}
			

		});

	}
	
}
