package com.mygdx.actors;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;
import com.mygdx.utils.MapData;

public class BackGround extends Actor{
	
	private Texture tile, selectedTile;
	private Image bgImage;
	
	
	private MapData mapData = new MapData();
	private SpriteBatch b = new SpriteBatch();
	private Texture[] tileType = new Texture[3];
	private static List<Vector2> nodesList;
	private static Sprite[][] sprites;

	private int tileX = Constants.TILE_X;
	private int tileY = Constants.TILE_Y;
	
	private int[][] spritesIndex;
	
	private int[][] spritesProperties;
			
	private int spritesX, spritesY;
	private int numberOfNodes = 0;
	
	private static BackGround instance;
	
	private BackGround(){
		super();
		spritesIndex = mapData.getData();
		spritesProperties = spritesIndex;//sprite properties, can construct tower? is it path? etc.
		tile = Assets.getAssetManager().get("path.png", Texture.class);
		bgImage = new Image (Assets.getAssetManager().get("playBg.jpg", Texture.class));
		bgImage.setBounds( 0, 0, Constants.APP_WIDTH, Constants.APP_HEIGHT);
		selectedTile = Assets.getAssetManager().get("tile.png", Texture.class);
		spritesX = spritesIndex.length;
		spritesY = spritesIndex[0].length;
		nodesList = new ArrayList<Vector2>();
		sprites = new Sprite[spritesX][spritesY];
		tileType[0] = tile;
		tileType[1] = selectedTile;
		addTilesToArray();
		setNodesfromMap(spritesIndex);
		
	}
	
	public static synchronized BackGround getInstance() {
		if(instance == null){
			instance = new BackGround();
		}
		
		return instance;
	}
	private Random rand = new Random();
	public void addTilesToArray(){
		
		for(int x = 0; x < spritesX; x++){
			for(int y = 0; y < spritesY; y++){
		
				if(spritesIndex[x][y] == -1){
					sprites[x][y] = new Sprite(tileType[1]);
					sprites[x][y].setPosition(x*tileX,  Constants.MAP_BOTTOM + y * tileY);
					sprites[x][y].setSize(tileX, tileY);
				}
				
				if(!(spritesIndex[x][y] == -1)){
					sprites[x][y] = new Sprite(tileType[0]);
					sprites[x][y].setColor(rand.nextFloat(),rand.nextFloat(), rand.nextFloat(), 0.8f);
					sprites[x][y].setPosition(x*tileX,  Constants.MAP_BOTTOM + y * tileY);
					sprites[x][y].setSize(tileX, tileY);
					
					if(!(spritesIndex[x][y] == 0)){
						numberOfNodes++;
					}
				}
			
				
			
			}
		}
	}
	
	
	//test
	private void setNodesfromMap(int[][] map){
		
		loop:
		for(int node = 1; node <= numberOfNodes; node++){
			
			for(int x = 0; x < spritesX; x ++ ){
				for(int y = 0; y < spritesY; y++){
					
					if(map[x][y] == node){
						nodesList.add(new Vector2(x * tileX,  Constants.MAP_BOTTOM + y * tileY));
						continue loop;
					}
									
				}
			}
			
		}
	}
	
	public int[][] getSpritesProp(){
		return spritesProperties;
	}
	public Sprite[][] getSpriteMap(){
		return sprites;
	}
	public List<Vector2> getNodesfromMap(){
		return nodesList;
	}
	
	public void reset(){
		nodesList = new ArrayList<Vector2>();
		sprites =  new Sprite[spritesX][spritesY];
		spritesIndex = mapData.getData();
		spritesProperties = spritesIndex;
		addTilesToArray();	
		setNodesfromMap(spritesProperties);
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		bgImage.draw(batch, parentAlpha);
		for(int y = 0; y < spritesY; y++){
			for(int x = 0; x < spritesX; x++){
				sprites[x][y].draw(batch);
			
			}
		}
	}
	@Override
	public boolean remove(){
		super.remove();
		//reset();
		instance = null;
		return true;
	}

	

}
