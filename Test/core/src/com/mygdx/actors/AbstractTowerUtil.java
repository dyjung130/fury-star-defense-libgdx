package com.mygdx.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.handler.ResearchUIHandler;
import com.mygdx.stage.TopStage;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;
import com.mygdx.utils.EffectType;

public abstract class AbstractTowerUtil extends AbstractTower implements ResearchUIHandler{
	
	public int towerLevel = 1; // tower level starts at 1
	public byte towerID = Constants.UTIL_1;
	public int xPos = 150, yPos = 200 , width = 50, height = 45, centerX, centerY; //default value
	public int cost, baseCost, collectible, baseCollectible;
	private float cdTime, totalTime;
	public short towerAngle;
	private boolean isReady, drawEffect;
	public EffectType effect;
	//status icon where it will show whether the tower can be activated or now
	private Image status = new Image(Assets.getAssetManager().get("badlogic.jpg", Texture.class));
	public ParticleEffect pe;
	public float collectingRate = Constants.collectibleRate;
	
	public AbstractTowerUtil(Vector2 actorPos, Vector2 actorDim, int cost, float cdTime, EffectType effectType){
		super();
		xPos = (int)actorPos.x;
		yPos = (int)actorPos.y;
		width = (int) actorDim.x;
		height = (int) actorDim.y;
		centerX = xPos + width/2;
		centerY = yPos + height/2;
		this.cdTime = cdTime;
		this.cost = cost;
		baseCost = cost;
		setBounds(xPos, yPos, width, height);
		effect = effectType;
		
		//particle effect
		pe = new ParticleEffect();
		pe.load(Gdx.files.internal("Effect/star.p"),Gdx.files.internal("Effect"));
		pe.scaleEffect(3f);
		pe.getEmitters().first().setPosition(centerX , centerY);
		pe.setPosition(centerX, centerY);
		pe.start();
		
		//ready status
		status.setBounds(centerX-width/4, yPos, width/2, height/4);
		status.setColor(Color.RED);
		actorOnTouchSetup();
	}
	
	private void actorOnTouchSetup(){
		addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				super.touchUp(event, x, y, pointer, button);
				if(isReady){
					drawEffect = true;
					towerActivated();
					isReady = false;
					status.setVisible(false);
				}
				
			}
		});
	}
	
	//collect either coin & score or apply effect 
	public abstract void towerActivated();
	
	
	
	//on touchdown upgrade all properties by 10 %?
	//upgrade will be done for each tower
	@Override
	public void upgrade(){
		if(Constants.towerOption == true){
			if(TopStage.getInstance().getMoney() - cost > 0){
				towerLevel += 1;
				collectible = (int)(baseCollectible * Math.pow( collectingRate, towerLevel-1));
				cost = (int)(baseCost * Math.pow( Constants.upgradeRate, towerLevel-1));
				TopStage.getInstance().updateMoney(cost);
			
			}else{

			}
		}
	}
		
	@Override
	public void act(float delta){
		super.act(delta);
		if(isReady == false){
			totalTime +=delta;
		}
		if(totalTime > cdTime){
			isReady = true;
			totalTime = 0;
		}
			
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		if(drawEffect){
			pe.draw(batch);
			pe.update(Gdx.graphics.getDeltaTime());
		}
		if(pe.isComplete()){
			drawEffect = false;
			pe.reset();
		}
		if(isReady){
			status.setVisible(true);
			status.draw(batch, parentAlpha);
		}
	}
	
}
