package com.mygdx.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class AbstractProjectile extends Actor{

	private Texture projectile;
	private Vector2 targetLoc;
	public Vector2 currentPos, tempCurrentPos;//projectile  position
	private float directionFactorX, directionFactorY;
	private float xDiff, yDiff, diffTCX, diffTCY;
	private int speed;
	private boolean didHit= false;
	
	public AbstractProjectile(Vector2 targetPos, Vector2 startPos, Texture projectile, int speed){
		super();
		this.projectile = projectile;
		this.speed = speed;
		targetLoc = targetPos; // initialize initial target position 
		currentPos = startPos;//initialize current projectile position
		
		tempCurrentPos = startPos;
	}
	
	//calculating projectile position
	private void calculateTraj(float delta, int speed){
		
		/*difference */
		diffTCX = targetLoc.x - currentPos.x;
		diffTCY = targetLoc.y - currentPos.y;
		
		/*distance */
		xDiff = Math.abs(diffTCX);
		yDiff = Math.abs(diffTCY);
	
		if(yDiff <= 25 && xDiff <= 25){
			didHit = true;
		}
		
		/*direction */
		if( xDiff > yDiff ){
			directionFactorY = yDiff / xDiff; 
			
			if((int)xDiff > 1){
				directionFactorX = 1;
			}else{
				directionFactorX = 0;
			}
		
		}else if(xDiff < yDiff ){

			directionFactorX = xDiff / yDiff;
			
			if((int)yDiff > 0){
				directionFactorY = 1;
			}else{
				directionFactorY = 0;
			}
		}
	
		if(xDiff > 0){
			currentPos.x += (diffTCX/xDiff) * directionFactorX * speed * delta; //direction x speed factor
		}
		
		if(yDiff > 0){
			currentPos.y += (diffTCY/yDiff) * directionFactorY * speed * delta; // direction x speed factor
		}
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		if(isVisible() == true)
			calculateTraj(delta, speed);
		
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		
		if(didHit == false){
			if(isVisible() == false){
				setVisible(true);
				batch.draw(projectile, currentPos.x, currentPos.y, 30, 30);
			}
		}else{
			reset();
		
			/*clear();
			remove();*/
		}
	}
	
	private void reset(){
		currentPos = tempCurrentPos;
		targetLoc = null;
		//targetLoc.setZero();//reset to (0,0);
		setVisible(false);
	}
	
	public void repeat(Vector2 nextTargetLoc){
		targetLoc = nextTargetLoc;
		setVisible(true);
		didHit= false;
	}
	
	public boolean getDidHit(){
		return didHit;
	}
	
	@Override
	public void clear(){
		super.clear();
	}
	
	@Override
	public boolean remove(){
		return super.remove();
	}

}
