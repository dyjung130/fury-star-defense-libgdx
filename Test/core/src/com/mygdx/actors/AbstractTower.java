package com.mygdx.actors;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/*
 * this class will be the base tower for all towers
 * it is connected to Gdx.input.isTouched in the BottomStage
 */
public class AbstractTower extends Actor{

	private boolean isTouched = false;
	
	//block 
	{
		this.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				super.touchDown(event, x, y, pointer, button);
				isTouched = true;
				return false;
			}
			
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor){
				super.enter(event, x, y, pointer, fromActor);
				isTouched = true;
			}
		});
	}
	
	
	public boolean towerTouched(){
		return isTouched;
	}
	
	public void resetTowerTouched(){
		isTouched = false;
	}
}
