package com.mygdx.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.utils.Assets;

public class BitmapFontActor extends Actor{
	
	private BitmapFont bitmapFont;
	private String value = "", name = "", name2 ="";
	private float x, y; 
	private Image icon;
	
	//Icon with Name
	//Especially for EffectType Name**
		public BitmapFontActor(float x, float y, float width, float height, int startValue, Texture icon){
			super();
			this.icon = new Image(icon);
			this.icon.setBounds(x ,y ,height, height);
			
			bitmapFont= new BitmapFont(Gdx.files.internal("Skin/hiero.fnt"),
			         Gdx.files.internal("Skin/hiero.png"), false);
			this.x = x + height*8/7;
			this.y = y + height*5/7;
			
			
			bitmapFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
			bitmapFont.setScale(1.4f);
		//	bitmapFont.setColor(Color.YELLOW);
			value = String.valueOf(startValue);
			
			
			
		
		}
	//Especially for EffectType Name**
	public BitmapFontActor(float x, float y, float width, float height, String name){
		super();
		bitmapFont= new BitmapFont(Gdx.files.internal("Skin/hiero.fnt"),
		         Gdx.files.internal("Skin/hiero.png"), false);
		this.x = x;
		this.y = y;
		this.name2 = name;
		
		setHeight(height);
		setWidth(width);
		
		bitmapFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		bitmapFont.setScale(0.7f);
		//bitmapFont.setColor(Color.YELLOW);
	
	}
	
	public BitmapFontActor(float x, float y, float width, float height, int startValue, String name){
		super();
		bitmapFont= new BitmapFont(Gdx.files.internal("Skin/hiero.fnt"),
		         Gdx.files.internal("Skin/hiero.png"), false);
		this.x = x;
		this.y = y;
		this.name = name;
		
		setHeight(height);
		setWidth(width);
		
		bitmapFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		bitmapFont.setScale(0.7f);
	//	bitmapFont.setColor(Color.YELLOW);
		value = String.valueOf(startValue);
	}
	
	
	
	public void updateValue(int value){
		this.value = String.valueOf(value);
	}
	
	public void updateName2(String name2){
		this.name2 = name2;
	}
	
	public void setScale(float value){
		bitmapFont.setScale(value);
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		bitmapFont.draw(batch, name + "" + name2 + value, x, y);
		
		if(icon != null){
			icon.draw(batch, parentAlpha);
		}
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
	}
	
}
