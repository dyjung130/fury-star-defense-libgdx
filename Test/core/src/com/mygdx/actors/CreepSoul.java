package com.mygdx.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mygdx.utils.Assets;

public class CreepSoul extends Actor{
	Image soulImage;
	float timer = 0f;
	
	public CreepSoul(Vector2 tempV){
		super();
		soulImage = new Image(Assets.getAssetManager().get("monster/creepSoul.png", Texture.class));
		soulImage.setSize(50, 50);
		soulImage.setPosition(tempV.x, tempV.y);//hide it in the beginning
		soulImage.addAction(Actions.parallel(Actions.moveBy(0, 150, 3f), Actions.fadeOut(2.4f)));
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		soulImage.draw(batch, parentAlpha);
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		soulImage.act(delta);
		if(timer > 3f){
			clear();
			remove();
		}
		timer += delta;
	}
	
	@Override
	public void clear(){
		super.clear();
		soulImage.clear();
	}
	
	@Override
	public boolean remove(){
		super.remove();
		soulImage.remove();
		return true;
	}
}
