package com.mygdx.actors;


import java.util.List;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.handler.ResearchUIHandler;
import com.mygdx.stage.BottomStage;
import com.mygdx.stage.TopStage;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;
import com.mygdx.utils.EffectType;

public abstract class AbstractTowerArea extends AbstractTower implements ResearchUIHandler{

	public int towerLevel = 1; // tower level starts at 1
	public byte towerID = Constants.AREA_1;
	public int xPos = 150, yPos = 200 , width = 50, height = 45, centerX, centerY; //default value
	public int range, damage, cost, baseCost;
	public float fireRate, duration, tempDuration, coolDown, ultDuration = 3f, ultTimer;
	public short towerAngle;
	private boolean drawRange, drawEffect;
	private boolean ultActivated = false;

	private List<AbstractCreep> targetList;
	

	private float cdTotalTime, totalTime;
	public EffectType effectType;
	public ParticleEffect pe;
	
	private ShapeRenderer shapeRend;
	
	//status indicator - cooldown bar
	private Image cdBar;
	
	public AbstractTowerArea(Vector2 actorPos, Vector2 actorDim, int range, int damage, int cost, 
			float fireRate, float duration, float coolDown,  EffectType effectType){
		super();
	
		xPos = (int)actorPos.x;
		yPos = (int)actorPos.y;
		width = (int) actorDim.x;
		height = (int) actorDim.y;
		
		centerX = xPos + width/2;
		centerY = yPos + height/2;
		
		setBounds(xPos, yPos, width, height);
		
		//actor common attributes
		this.range = range;
		this.damage = damage;
		this.cost = cost;
		baseCost = cost;
		this.fireRate = fireRate;
		this.duration = duration;
		this.tempDuration = duration;
		this.coolDown = coolDown;
		this.effectType = effectType;
		
		//Particle Effect
		pe = new ParticleEffect();
		pe.load(Gdx.files.internal("Effect/slowEffect.p"),Gdx.files.internal("Effect"));
		pe.scaleEffect(1f);
		pe.getEmitters().first().setPosition(centerX , centerY);
		pe.setPosition(centerX, centerY);
		pe.start();
		   
		
		//projectileList = new ArrayList<AbstractProjectile>();
		shapeRend = new ShapeRenderer();
		cdBar = new Image(Assets.getAssetManager().get("badlogic.jpg", Texture.class));
		cdBar.setBounds(xPos+30, yPos-5, 0, 7);
		//actor listener * important
		actorIsTouched();
	}
	
	
	@Override
	public void upgrade(){
		if(Constants.towerOption == true){
			if(TopStage.getInstance().getMoney() - cost > 0){
				towerLevel +=1;
			//	range *= 1.15f;
			//	currentLevel += 1;
				damage +=1;
				cost = (int)(baseCost * Math.pow( Constants.upgradeAreaRate, towerLevel-1));
				TopStage.getInstance().updateMoney(cost);

				
			}else{

			}
		}
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		setTargetList();
		//effect duration
		if(duration > 0){
			applyEffect(targetList);
			drawEffect = true;
			duration -= delta;
		}else{
			drawEffect = false;
			
			pe.reset();
			totalTime += delta;
		}
		
		//controlling firerate based on elapsed time
		if(totalTime > fireRate){
			duration = tempDuration;
			totalTime = 0;
		}
		
		drawRangeOnClick();
		//check if ult is available
		checkUltAvailable(delta);
		
		
		if(cdTotalTime > 0){
			cdBar.setWidth((width-60) * cdTotalTime/coolDown);
		}
	}
	
	private boolean ultAvailable = false;
	
	
	private void checkUltAvailable(float delta){
		
		if(coolDown < cdTotalTime && ultAvailable == false){
			cdTotalTime = 0;
			ultAvailable = true;
			System.out.println("ult is available");
			//cdBar.setWidth(width);
		}
		
		if(ultAvailable == false){
			cdTotalTime += delta;
			//cdBar.setWidth(width * cdTotalTime/coolDown);
			System.out.println("ult is NOT available");
		}
		//activate ult when true
		if(ultActivated == true){
			if(ultTimer <=ultDuration){
				ultTimer += delta;
			}else{
				ultTimer = 0;
				ultActivated = false;
				//reduce to original value when ended
				range /= 1.5f;
				
			}
		}
	}
	
	public void setDrawRange(boolean value){
		drawRange = value;
	}

	public boolean getDrawRange(){
		return drawRange;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		cdBar.draw(batch, parentAlpha);
		
		if(drawEffect == true){
			
			pe.draw(batch);
			pe.update(Gdx.graphics.getDeltaTime());
		
		}
	}

	//* draw tower's range ontouch
	private void drawRangeOnClick(){
		
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA,GL20.GL_ONE_MINUS_SRC_ALPHA);
		shapeRend.setAutoShapeType(true);
		shapeRend.begin(ShapeType.Filled);
		shapeRend.setColor(new Color(1f, 0f, 0.1f, 0.5f));
		shapeRend.setProjectionMatrix(BottomStage.getInstance().getCamera().combined);//drawn to test stage coordinates
	
		if(drawRange == true){
			shapeRend.circle(this.centerX, this.centerY, this.range);
		}
		
		shapeRend.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);
		//* draw end
	}
	
	public void setTargetList(){

		targetList = BottomStage.getInstance().getTargetList();
	
	}
	
	//check if the target is in range
	public void applyEffect(List<AbstractCreep> eList){
		
		if(eList != null){// && tempList.size() <= numOfEnemies){
			for(AbstractCreep e: eList){
				if(e != null ){
					float tempX = e.getCurrentTargetPosition().x - this.centerX;
					float tempY = e.getCurrentTargetPosition().y - this.centerY;
				
					if(sqrt(tempX*tempX + tempY*tempY) < (range)){
						
						e.applyEffect(effectType);
						updateTargetHealth(e);
						towerAngle = (short) (Math.atan2(tempY,tempX) * 57.2f + 90);

					}
				}
			}
		}
	}

	private void updateTargetHealth(AbstractCreep tta){
		if(tta != null){
			tta.calculateHealth(true, damage);
		}
	}
	
	
	
	private static double sqrt(final double a){
		final long x = Double.doubleToLongBits(a) >> 32;
		double y = Double.longBitsToDouble((x + 1072632448) << 31);

		// repeat the following line for more precision
		//y = (y + a / y) * 0.5;
		return y;
	}
	
	
	//currently to check if range needs to be drawn
	private void actorIsTouched(){
		
		this.addListener(new ClickListener(){
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor){
			//	if(TopStage.botStageDragged == true)
				super.enter(event, x, y, pointer, fromActor);
				drawRange = true;
				
			}
			
			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor){
				super.exit(event, x, y, pointer, toActor);
				drawRange = false;

				if(Constants.towerOption == false){
					
					TopStage.botStageDragged = false;
					
					//activate ult on exit
					if(ultAvailable == true){
						ultAvailable = false;
						ultActivated = true;
						range *= 1.5f;
					
					}
				}
			}
		
		});

	}
	
}
