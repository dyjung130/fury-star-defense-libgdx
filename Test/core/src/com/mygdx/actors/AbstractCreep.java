package com.mygdx.actors;

import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mygdx.actors.BackGround;
import com.mygdx.utils.Assets;
import com.mygdx.utils.Constants;
import com.mygdx.utils.EffectType;

public abstract class AbstractCreep extends Actor{
	
	private float tile_Y0 = Constants.TILE_Y * 0.7f;
	private float tile_Y = Constants.TILE_Y * 0.13f;
	
	private Texture target, healthImg;
	private Image targetImage;
	//private BitmapFont healthPoint;
	private String healthBitmap = "";
	
	private Vector2 v = new Vector2(0,Constants.MAP_BOTTOM); //
	
	private List<Vector2> nodeAList;
	private Vector2[] nodeList;// = new Vector2[4];
	private int n = 0; //node counter
	
	private float health, totalHealth, movementSpeed, tempSpeed;
	private int gold, centerX = Constants.TILE_X/2, centerY =Constants.TILE_Y/2;
	private float totalTime;
	
	//effect duration
	private float slowTimer = 0f;
	private float frozenTimer = 0f;
	private float burnTimer = 0f;
	
	private boolean reachedDestination = false, isDestroyed = false;
	//effect 
	private boolean isFrozen = false, isSlowed = false, isBurnt = false, isArmorReduced = false, deathActivated = false;
	//particle effect
	public ParticleEffect pe;
	private Random rand = new Random();
	
	
	public AbstractCreep(float health, float movementSpeed, int gold, Texture target){
		super();
		this.health = health;
		this.totalHealth = health;
		this.movementSpeed = movementSpeed;
		this.gold = gold;
		this.tempSpeed  = movementSpeed;
		this.target = target;
		target.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		this.targetImage = new Image(target);
		targetImage.setBounds(0, 0, Constants.TILE_X, Constants.TILE_Y);
		targetImage.setOrigin(centerX, centerY);
		targetImage.addAction(Actions.forever(Actions.sequence(Actions.sizeBy(-10, -10, 1f), Actions.sizeBy(10, 10, 1f))));
	
		getAssets();
		setNodes();

		//particle effect
		pe = new ParticleEffect();
		
	}
	
	private void getAssets(){
		healthImg = Assets.getAssetManager().get("health.jpg", Texture.class);
	}

	//convert returned node arraylist to node array
	public void setNodes(){
		nodeAList = BackGround.getInstance().getNodesfromMap();
		nodeList = new Vector2[nodeAList.size()];
		nodeList = nodeAList.toArray(nodeList);
		//System.out.println(nodeAList.size());
	}
	
	private void changeDirection(float degrees){
		targetImage.setRotation(degrees);
	}
	
	//move target(imageTarget) and rotate target's direction
	private void moveTarget(float movementSpeed){
		if(n != nodeList.length){//test..
			for(; n < nodeList.length;){
				
				if((int)Math.abs(nodeList[n].x - v.x) >= 10){
					float tempX = (nodeList[n].x - v.x)/Math.abs(v.x - nodeList[n].x);
					v.x += tempX * movementSpeed ;
					
					if(tempX > 0){
						changeDirection(270); // right
					}
					
					if(tempX < 0){
						changeDirection(90); //  left
					}
				}
					
				if((int)Math.abs(nodeList[n].y - v.y) >= 10){
					float tempY = (nodeList[n].y - v.y)/Math.abs(v.y - nodeList[n].y);
					v.y +=  tempY * movementSpeed;
					
					if(tempY > 0){
						changeDirection(0); // up
					}
					
					if(tempY < 0){
						changeDirection(180); // down
					}
				}
					
				if((int)Math.abs(nodeList[n].x - v.x) <= 10 && (int)Math.abs(nodeList[n].y - v.y) <= 10){
					n ++;
				}
				
				targetImage.setPosition(v.x, v.y);
				break;
			}
		
		}else{
			reachedDestination = true;
		//	healthPoint.dispose();
		}
	
	}
	
	
	public boolean isReachedToDest(){
		return reachedDestination;
	}
	
	//all timer must be "less than 0" not "less than equals to 0"
	private void checkCurrentEffect(float delta){
		if(isSlowed == true){
			slowTimer-=delta;
			
			if(slowTimer < 0){
				isSlowed = false;
				movementSpeed = tempSpeed;
			}
			
		}
	
		if(isFrozen == true){
			frozenTimer -=delta;

			if(frozenTimer < 0){
				isFrozen = false;
				movementSpeed = tempSpeed;
			}
		}
		
		if(isBurnt == true){
			burnTimer -=delta;
			health = health * 0.995f;
			
			if(burnTimer < 0){
				isBurnt = false;
				
			}
		}
		
	}
	
	
	public void applyEffect(EffectType e){
		float multiplier = e.getMultiplier();
		float timer = e.getDuration();
		//System.out.println("effect Applied called");
		switch(e.getId()){
			case Constants.freeze:
				if(isFrozen == false){
					movementSpeed *=multiplier;
					frozenTimer  = timer;
					isFrozen = true;
					pe.load(Gdx.files.internal("Effect/frozen.p"),Gdx.files.internal("Effect"));
					pe.scaleEffect(0.7f);
					pe.getEmitters().first().setPosition(v.x, v.y);
					pe.setPosition(v.x, v.y);
					pe.start();
				}
				break;
			case Constants.slow:
				//correct way
				if(isSlowed == false){
					movementSpeed *= multiplier;
					slowTimer = timer;
					isSlowed = true;
					System.out.println("slow called: " + multiplier);
					
				}
				break;
			case Constants.armorReducer:
				if(isArmorReduced != true){
					health = (isArmorReduced != false) ? (health * multiplier): health;
					isArmorReduced =true;
				}
				break;
			case Constants.armorReducerv2:
				if(isArmorReduced != true){
					health = (isArmorReduced != false) ? (health * multiplier): health;
					isArmorReduced =true;
				}
				break;
			case Constants.burn:
				isBurnt = true;
				burnTimer = timer;
				
				break;
			case Constants.teleport:
				if(n > 1){
					n = n - 2;
				}
				break;
			case Constants.instantD:
			
				if(rand.nextInt(13)==7){
					deathActivated = true;
					health = -10;
					isDestroyed = true;
					System.out.println("death activated");
				}
				break;
			default:
				break;
		}
	}
	
	public void calculateHealth(int damage){
		if(health >= 0){
			health -= damage;
		}
	
		if(health < 3){
			isDestroyed = true;
			//healthPoint.dispose();
			
			targetImage = null;
			healthImg = null;
			target = null;
			
		}
		

	}
	
	public void calculateHealth(boolean isHit, int damage){
		if(isHit == true && health >= 0){
			health -= damage;
		}
	
		if(health <= 3){
			isDestroyed = true;
			targetImage = null;
			healthImg = null;
			target = null;
		}
	}
	
	public boolean isTargetDestroyed(){
		return isDestroyed;
	}
	
	public float getCurrentHealth(){
		return health;
	}
		
	public int getGold(){
		return gold;
	}
	
	
	@Override
	public void act(float delta){
		super.act(delta);
		targetImage.act(delta);
		checkCurrentEffect(delta);
		moveTarget(delta*movementSpeed);
		healthBitmap = health*100/totalHealth + " %";
	}
	
	private void drawStatus(Batch batch, float parentAlpha){
		if(isFrozen == true){
			pe.draw(batch);
			pe.update(Gdx.graphics.getDeltaTime());
		}else{
			if(pe.isComplete()){
				pe.reset();
			}
		}
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		if(health > 0){
			targetImage.draw(batch, parentAlpha);
			//batch.draw(target, v.x, v.y, Constants.TILE_X, Constants.TILE_Y);
			batch.draw(healthImg, v.x, v.y + tile_Y0, Constants.TILE_X * health/totalHealth, tile_Y);
			//healthPoint.draw(batch, healthBitmap , v.x, v.y + 60);
			drawStatus(batch, parentAlpha);
		}
	}
	
	public Vector2 getCurrentTargetPosition(){
		return new Vector2(v.x+centerX , v.y+centerY);
	}
	
	@Override 
	public boolean remove(){
		return super.remove();
	}
	
	@Override 
	public void clear(){
		super.clear();
		target = null;
		healthImg = null;
	}
}

