Untitled
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 0
max: 500
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 300.0
relative: true
scalingCount: 6
scaling0: 1.0
scaling1: 0.036363635
scaling2: 0.6545454
scaling3: 0.14545454
scaling4: 0.0
scaling5: 0.0
timelineCount: 6
timeline0: 0.0
timeline1: 0.2260274
timeline2: 0.24657534
timeline3: 0.37671232
timeline4: 0.6232877
timeline5: 0.96575344
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 1000.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.09090909
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.3561644
timeline2: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: true
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 20.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 20.0
highMax: 20.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 32.0
highMax: 32.0
relative: false
scalingCount: 14
scaling0: 0.0
scaling1: 0.47272727
scaling2: 1.0
scaling3: 0.0
scaling4: 0.7090909
scaling5: 0.036363635
scaling6: 0.5090909
scaling7: 0.036363635
scaling8: 0.36363637
scaling9: 0.054545455
scaling10: 0.29090908
scaling11: 0.0
scaling12: 0.0
scaling13: 0.0
timelineCount: 14
timeline0: 0.0
timeline1: 0.12328767
timeline2: 0.1780822
timeline3: 0.24657534
timeline4: 0.369863
timeline5: 0.44520548
timeline6: 0.5273973
timeline7: 0.53424656
timeline8: 0.67808217
timeline9: 0.70547944
timeline10: 0.7808219
timeline11: 0.8630137
timeline12: 0.94520545
timeline13: 0.9794521
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 6
scaling0: 0.25454545
scaling1: 1.0
scaling2: 0.5272727
scaling3: 0.29090908
scaling4: 0.14545454
scaling5: 0.054545455
timelineCount: 6
timeline0: 0.0
timeline1: 0.2260274
timeline2: 0.2739726
timeline3: 0.48630136
timeline4: 0.74657536
timeline5: 0.9520548
- Angle - 
active: false
- Rotation - 
active: true
lowMin: 1.0
lowMax: 360.0
highMin: 360.0
highMax: 360.0
relative: false
scalingCount: 10
scaling0: 0.9818182
scaling1: 0.4
scaling2: 0.7818182
scaling3: 0.29090908
scaling4: 0.7090909
scaling5: 0.2
scaling6: 0.6727273
scaling7: 0.12727273
scaling8: 0.5272727
scaling9: 1.0
timelineCount: 10
timeline0: 0.0
timeline1: 0.15068494
timeline2: 0.36301368
timeline3: 0.47945204
timeline4: 0.5684931
timeline5: 0.7328767
timeline6: 0.7808219
timeline7: 0.8561644
timeline8: 0.93835616
timeline9: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.62352943
colors1: 0.25882354
colors2: 0.47843137
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 6
scaling0: 1.0
scaling1: 1.0
scaling2: 1.0
scaling3: 0.75438595
scaling4: 0.75
scaling5: 0.0
timelineCount: 6
timeline0: 0.0
timeline1: 0.2
timeline2: 0.46575344
timeline3: 0.5958904
timeline4: 0.8
timeline5: 1.0
- Options - 
attached: false
continuous: false
aligned: true
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
blast.png
