Untitled
- Delay -
active: false
- Duration - 
lowMin: 2000.0
lowMax: 2000.0
- Count - 
min: 10
max: 30
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 300.0
relative: true
scalingCount: 10
scaling0: 1.0
scaling1: 0.21818182
scaling2: 0.90909094
scaling3: 0.09090909
scaling4: 0.76363635
scaling5: 0.10909091
scaling6: 0.4
scaling7: 0.25454545
scaling8: 0.14545454
scaling9: 0.054545455
timelineCount: 10
timeline0: 0.0
timeline1: 0.08219178
timeline2: 0.10273973
timeline3: 0.21917808
timeline4: 0.34246576
timeline5: 0.5273973
timeline6: 0.58219177
timeline7: 0.6369863
timeline8: 0.7123288
timeline9: 0.9726027
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: true
scalingCount: 9
scaling0: 1.0
scaling1: 0.54545456
scaling2: 0.58181816
scaling3: 0.54545456
scaling4: 0.34545454
scaling5: 0.6363636
scaling6: 0.3272727
scaling7: 0.6545454
scaling8: 0.0
timelineCount: 9
timeline0: 0.0
timeline1: 0.15753424
timeline2: 0.31506848
timeline3: 0.44520548
timeline4: 0.5410959
timeline5: 0.56164384
timeline6: 0.65068495
timeline7: 0.69863015
timeline8: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: true
scalingCount: 10
scaling0: 1.0
scaling1: 0.10909091
scaling2: 0.8363636
scaling3: 0.18181819
scaling4: 0.56363636
scaling5: 0.2
scaling6: 1.0
scaling7: 0.34545454
scaling8: 0.96363634
scaling9: 0.29090908
timelineCount: 10
timeline0: 0.0
timeline1: 0.0890411
timeline2: 0.15753424
timeline3: 0.24657534
timeline4: 0.34246576
timeline5: 0.39726028
timeline6: 0.46575344
timeline7: 0.63013697
timeline8: 0.739726
timeline9: 0.82191783
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 45.0
highMax: 45.0
relative: true
scalingCount: 10
scaling0: 0.0
scaling1: 0.8181818
scaling2: 0.6181818
scaling3: 0.4
scaling4: 0.23636363
scaling5: 0.2
scaling6: 0.14545454
scaling7: 0.0
scaling8: 0.0
scaling9: 0.0
timelineCount: 10
timeline0: 0.0
timeline1: 0.1369863
timeline2: 0.21917808
timeline3: 0.38941097
timeline4: 0.5058493
timeline5: 0.61543834
timeline6: 0.74557537
timeline7: 0.80036986
timeline8: 0.89041096
timeline9: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.69411767
colors1: 0.4392157
colors2: 0.05882353
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 1.0
scaling1: 0.9298246
scaling2: 0.57894737
scaling3: 0.21052632
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.1849315
timeline2: 0.4041096
timeline3: 0.45890412
timeline4: 1.0
- Options - 
attached: true
continuous: false
aligned: true
additive: true
behind: true
premultipliedAlpha: false
- Image Path -
star.png
