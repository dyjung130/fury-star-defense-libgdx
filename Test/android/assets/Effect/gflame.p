Untitled
- Delay -
active: false
- Duration - 
lowMin: 1500.0
lowMax: 1500.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.33561644
timeline2: 0.6232877
timeline3: 0.9041096
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 1000.0
relative: true
scalingCount: 9
scaling0: 1.0
scaling1: 0.72727275
scaling2: 0.8909091
scaling3: 0.8181818
scaling4: 0.0
scaling5: 1.0
scaling6: 0.12727273
scaling7: 1.0
scaling8: 1.0
timelineCount: 9
timeline0: 0.0
timeline1: 0.14383562
timeline2: 0.21232876
timeline3: 0.25342464
timeline4: 0.369863
timeline5: 0.4178082
timeline6: 0.53424656
timeline7: 0.6917808
timeline8: 0.9315069
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 50.0
lowMax: 50.0
highMin: 80.0
highMax: 80.0
relative: true
scalingCount: 3
scaling0: 0.18181819
scaling1: 0.4181818
scaling2: 0.8
timelineCount: 3
timeline0: 0.0
timeline1: 0.46575344
timeline2: 0.67808217
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: 360.0
relative: true
scalingCount: 10
scaling0: 1.0
scaling1: 0.9325843
scaling2: 0.8727273
scaling3: 0.73595506
scaling4: 0.6181818
scaling5: 0.54545456
scaling6: 0.47272727
scaling7: 0.38181818
scaling8: 0.2
scaling9: 0.0
timelineCount: 10
timeline0: 0.0
timeline1: 0.08159393
timeline2: 0.10958904
timeline3: 0.18026565
timeline4: 0.2665522
timeline5: 0.34146577
timeline6: 0.44520548
timeline7: 0.51369864
timeline8: 0.6849315
timeline9: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.4117647
colors1: 0.84705883
colors2: 0.3019608
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 7
scaling0: 0.0
scaling1: 0.2982456
scaling2: 0.5263158
scaling3: 1.0
scaling4: 0.54385966
scaling5: 0.28070176
scaling6: 0.0
timelineCount: 7
timeline0: 0.0
timeline1: 0.29452056
timeline2: 0.39726028
timeline3: 0.53424656
timeline4: 0.65068495
timeline5: 0.7328767
timeline6: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: true
premultipliedAlpha: false
- Image Path -
flame.png
