Untitled
- Delay -
active: false
- Duration - 
lowMin: 2000.0
lowMax: 2000.0
- Count - 
min: 0
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: true
scalingCount: 9
scaling0: 1.0
scaling1: 0.45454547
scaling2: 0.94545454
scaling3: 0.4
scaling4: 0.6727273
scaling5: 0.3090909
scaling6: 0.25454545
scaling7: 0.76363635
scaling8: 0.9818182
timelineCount: 9
timeline0: 0.0
timeline1: 0.13013698
timeline2: 0.25342464
timeline3: 0.39726028
timeline4: 0.5273973
timeline5: 0.6369863
timeline6: 0.70547944
timeline7: 0.8356164
timeline8: 0.9931507
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 1000.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 0.07272727
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.26027396
timeline2: 0.63013697
timeline3: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: true
scalingCount: 9
scaling0: 1.0
scaling1: 1.0
scaling2: 1.0
scaling3: 1.0
scaling4: 1.0
scaling5: 1.0
scaling6: 1.0
scaling7: 1.0
scaling8: 1.0
timelineCount: 9
timeline0: 0.0
timeline1: 0.15068494
timeline2: 0.28082192
timeline3: 0.4178082
timeline4: 0.5273973
timeline5: 0.6369863
timeline6: 0.7191781
timeline7: 0.9109589
timeline8: 1.0
- Velocity - 
active: true
lowMin: 19.0
lowMax: 19.0
highMin: 131.0
highMax: 131.0
relative: false
scalingCount: 9
scaling0: 1.0
scaling1: 0.6
scaling2: 0.8727273
scaling3: 0.34545454
scaling4: 0.7818182
scaling5: 0.10909091
scaling6: 0.76363635
scaling7: 0.47272727
scaling8: 0.8909091
timelineCount: 9
timeline0: 0.0
timeline1: 0.16438356
timeline2: 0.24657534
timeline3: 0.3219178
timeline4: 0.39726028
timeline5: 0.44520548
timeline6: 0.55479455
timeline7: 0.6643836
timeline8: 0.96575344
- Angle - 
active: true
lowMin: 280.0
lowMax: 280.0
highMin: 360.0
highMax: 200.0
relative: true
scalingCount: 13
scaling0: 1.0
scaling1: 0.36363637
scaling2: 0.8727273
scaling3: 0.47272727
scaling4: 0.6727273
scaling5: 0.8363636
scaling6: 0.34545454
scaling7: 0.7818182
scaling8: 0.38181818
scaling9: 0.8909091
scaling10: 0.14545454
scaling11: 0.7090909
scaling12: 0.0
timelineCount: 13
timeline0: 0.0
timeline1: 0.034246575
timeline2: 0.10958904
timeline3: 0.21917808
timeline4: 0.28767124
timeline5: 0.369863
timeline6: 0.5
timeline7: 0.60273975
timeline8: 0.6917808
timeline9: 0.77397263
timeline10: 0.8561644
timeline11: 0.93835616
timeline12: 1.0
- Rotation - 
active: true
lowMin: 1.0
lowMax: 1.0
highMin: 360.0
highMax: 360.0
relative: true
scalingCount: 8
scaling0: 0.96363634
scaling1: 0.7818182
scaling2: 0.07272727
scaling3: 0.6909091
scaling4: 0.10909091
scaling5: 0.8363636
scaling6: 0.3272727
scaling7: 0.18181819
timelineCount: 8
timeline0: 0.0
timeline1: 0.15068494
timeline2: 0.21917808
timeline3: 0.28082192
timeline4: 0.39041096
timeline5: 0.51369864
timeline6: 0.7123288
timeline7: 0.8287671
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 0.42352942
colors2: 0.37254903
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 6
scaling0: 1.0
scaling1: 1.0
scaling2: 1.0
scaling3: 1.0
scaling4: 0.8596491
scaling5: 0.0
timelineCount: 6
timeline0: 0.0
timeline1: 0.1780822
timeline2: 0.25342464
timeline3: 0.47945204
timeline4: 0.6438356
timeline5: 1.0
- Options - 
attached: true
continuous: true
aligned: true
additive: true
behind: true
premultipliedAlpha: false
- Image Path -
explosive.png
