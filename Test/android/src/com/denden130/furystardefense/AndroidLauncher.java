package com.denden130.furystardefense;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.denden130.furystardefense.Main;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.mygdx.handler.AndroidCallbackHandler;
import com.mygdx.handler.GoogleServicesHandler;
import com.mygdx.player.MyPlayer;

public class AndroidLauncher extends AndroidApplication implements
		AndroidCallbackHandler, GoogleServicesHandler {
	private final static int REQUEST_CODE_UNUSED = 9002;
	private final static int RC_SAVED_GAMES = 9009;
	private static int highestScore;

	private AudioManager audioManager;
	private GameHelper gameHelper;
	private Main mainGame;

	// Admob instances
	protected AdView adView;
	protected View gameView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// add request feature before super --> reference:
		// (https://github.com/JakeWharton/ActionBarSherlock/issues/310)
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		initializeGameHelper(); // Google Play Services

		if (mainGame == null) {
			mainGame = new Main(this);
		}

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		// initialize(mainGame, config); previously used to initialize

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

		RelativeLayout layout = new RelativeLayout(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);
		layout.setLayoutParams(params);
		AdView admobView = createAdView();
		layout.addView(admobView);
		View gameView = createGameView(config);
		layout.addView(gameView);

		setContentView(layout);
		startAdvertising(admobView);

		// Set callback between this and Main
		mainGame.setCallBack(this);
		// Audio Manager...

		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	}

	private AdView createAdView() {
		adView = new AdView(this);
		adView.setAdSize(AdSize.SMART_BANNER);
		adView.setAdUnitId(getString(R.string.banner_ad_unit_id));
		adView.setId(777); // this is an arbitrary id, allows for relative
							// positioning in createGameView()
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
		adView.setLayoutParams(params);
		adView.setBackgroundColor(Color.BLACK);
		return adView;
	}

	private View createGameView(AndroidApplicationConfiguration cfg) {
		gameView = initializeForView(mainGame, cfg);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.BELOW, adView.getId());
		gameView.setLayoutParams(params);
		return gameView;
	}

	private void startAdvertising(AdView adView) {
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);
	}

	/* end google admob.. */

	private void initializeGameHelper() {
		// TODO Auto-generated method stub
		gameHelper = new GameHelper(this, GameHelper.CLIENT_ALL); // Client_All
																	// includes
																	// (1) plus
																	// scope
																	// game (2)
																	// drive (3)
																	// appstate
																	// (4) plus
		gameHelper.enableDebugLog(false);

		GameHelperListener gameHelperListener = new GameHelperListener() {

			@Override
			public void onSignInFailed() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onSignInSucceeded() {
				// TODO Auto-generated method stub

			}

		};
		gameHelper.setMaxAutoSignInAttempts(0);
		gameHelper.setup(gameHelperListener);

		showSavedGamesUI();
	}

	@Override
	protected void onStart() {
		super.onStart();
		gameHelper.onStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		gameHelper.onStop();
	}

	@Override
	protected void onPause() {
		if (adView != null)
			adView.pause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (adView != null)
			adView.resume();
	}

	@Override
	protected void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// this code is prevent google sign out from the leaderboard UI in game

		if (resultCode == GamesActivityResultCodes.RESULT_RECONNECT_REQUIRED) {
			gameHelper.disconnect();
		} else {
			gameHelper.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public void callBack(float volumeSet) {
		// TODO Auto-generated method stub
		if (volumeSet > 0) {
			audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
					AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
		}

		if (volumeSet < 0) {
			audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
					AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
		}

	}

	@Override
	public void signIn() {
		// TODO Auto-generated method stub
		try {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					gameHelper.beginUserInitiatedSignIn();
				}

			});
		} catch (Exception e) {
			Gdx.app.log("AndroidLauncher", "Log in failed");
		}
	}

	@Override
	public void signOut() {
		// TODO Auto-generated method stub
		try {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					gameHelper.signOut();
				}

			});
		} catch (Exception e) {
			Gdx.app.log("AndroidLauncher", "Log out failed");
		}
	}

	@Override
	public void rateGame() {
		// opens google app store on new activity
		String str = "https://play.google.com/store/apps/details?id=com.denden130.furystardefense";
		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(str)));
	}

	@Override
	public void submitScore(int score) {
		// TODO Auto-generated method stub
		if (isSignedIn() == true) {
			Games.Leaderboards.submitScore(gameHelper.getApiClient(),
					getString(R.string.lb_id), score);
			startActivityForResult(Games.Leaderboards.getLeaderboardIntent(
					gameHelper.getApiClient(), getString(R.string.lb_id)),
					REQUEST_CODE_UNUSED);
		} else {
			// try user to sign in or sign up and then submit score
		}
	}

	@Override
	public void showScore() {
		// TODO Auto-generated method stub
		startActivityForResult(
				Games.Leaderboards.getLeaderboardIntent(
						gameHelper.getApiClient(), getString(R.string.lb_id)),
				REQUEST_CODE_UNUSED);
	}

	@Override
	public boolean isSignedIn() {
		// TODO Auto-generated method stub
		return gameHelper.isSignedIn();
	}

	// Shows default saved games UI Selection Google Saved Game
	@Override
	public void showSavedGamesUI() {
		// TODO Auto-generated method stub
		int maxNumOfSavedGameToShow = 3;
		if (isSignedIn() == true) {
			startActivityForResult(Games.Snapshots.getSelectSnapshotIntent(
					gameHelper.getApiClient(), getString(R.string.my_save),
					true, true, maxNumOfSavedGameToShow), RC_SAVED_GAMES);
			System.out.println("saved game load called");
		} else {
			System.out.println("saved game load not called");
			// option to show other screen if loading isn't psosible
			// or connection error? provide sign in option
		}
	}

	// needs async
	@Override
	public MyPlayer loadFromLocalGame(MyPlayer player, String fileName) {
		// TODO Auto-generated method stub
		Preferences pref = Gdx.app.getPreferences(fileName);

		return player;
	}

	// needs async
	@Override
	public void saveToLocalGame(MyPlayer player, String fileName) {
		// TODO Auto-generated method stub
		Preferences pref = Gdx.app.getPreferences(fileName);

		// call flush to save changes
		pref.flush();
	}

	@Override
	public int getHighestScore() {
		// TODO Auto-generated method stubgameHelper.getApiClient(),
		// getString(R.string.lb_id)), REQUEST_CODE_UNUSED
		if (isSignedIn() == true) {
			Games.Leaderboards.loadCurrentPlayerLeaderboardScore(
					gameHelper.getApiClient(), getString(R.string.lb_id),
					LeaderboardVariant.TIME_SPAN_ALL_TIME,
					LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(
					new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {
						@Override
						public void onResult(
								final Leaderboards.LoadPlayerScoreResult scoreResult) {
							// TODO Auto-generated method stub
							if (isScoreResultValid(scoreResult)) {
								highestScore = (int) scoreResult.getScore()
										.getRawScore();
							}
						}
					});
		}
		return highestScore;
	}

	private boolean isScoreResultValid(
			final Leaderboards.LoadPlayerScoreResult scoreResult) {
		return scoreResult != null
				&& GamesStatusCodes.STATUS_OK == scoreResult.getStatus()
						.getStatusCode() && scoreResult.getScore() != null;
	}
}
