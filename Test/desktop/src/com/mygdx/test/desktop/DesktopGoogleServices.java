package com.mygdx.test.desktop;

import com.mygdx.handler.GoogleServicesHandler;

public class DesktopGoogleServices implements GoogleServicesHandler{

	@Override
	public void signIn()
	{
//	System.out.println("DesktopGoogleServies: signIn()");
	}

	@Override
	public void signOut()
	{
//	System.out.println("DesktopGoogleServies: signOut()");
	}

	@Override
	public void rateGame()
	{
//	System.out.println("DesktopGoogleServices: rateGame()");
	}

	@Override
	public void submitScore(int score)
	{
//	System.out.println("DesktopGoogleServies: submitScore(" + score + ")");
	}

	@Override
	public void showScore()
	{
//	System.out.println("DesktopGoogleServies: showScores()");
	}

	@Override
	public boolean isSignedIn()
	{
//	System.out.println("DesktopGoogleServies: isSignedIn()");
	return false;
	}

	@Override
	public void showSavedGamesUI() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getHighestScore() {
		// TODO Auto-generated method stub
		return 0;
	}
}
